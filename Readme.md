1. Установить Java 8
2. Скачать и запустить neo4j  
	2.1 Скачать neo4j community edition (надо брать последнюю стабильную сборку)
		http://neo4j.com/download/other-releases/ (Рекомендуется скачивать в формате zip )  
	2.2 Запустить bin\Neo4j.bat  
	2.3 После старта БД  зайти localhost:7474 (neo4j/neo4j)  
	2.4 neo4j попросит поменять пароль, ввести новый: secr8t  
3. Взять исходники отсюда (git) https://bitbucket.org/DMitin/corporate-portal-server
4. Создать новый проект в IDEA (maven)
5. Запуск проекта в IDEA  
	5.1 edit configuration -> new application  
	5.2 Ввести параметры  
		Main class: io.vertx.core.Starter  
		VM options: -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory  
		Program arguments: run portal.Server -conf app.conf 
		Working directory: (папка поректа)  
		Use classpath of module: название проекта (по умолчанию corporte-portal-server)  
		JRE: 1.8  
6. Проверка запуска  
	6.1 Проверить что в консоли есть строки:  
	.....  
	INFO  Server - стартую сервер на порту: 8080  
	.....  
	INFO  portal.neo4j.Neo4j - Portal подключен к neo4j  
7. Запуск rest запросов  
	7.1 Поставить Chrome plugin: postman  
	7.2 Запустить постман и импортировать проект  
		- Import -> Import Folder (!!! это важно не file а folder)  
		- Выбрать папку raml (находится в git corporate-portal-server)  
		- Должно вылезти сообщение что коллекция импортирована  

	!!! в Postman есть ошибка при загрузке файла, так что для метода loadImage нужно использовать другой rest клиент