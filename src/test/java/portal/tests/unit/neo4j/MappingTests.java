package portal.tests.unit.neo4j;

import org.junit.Test;

import java.io.IOException;

public class MappingTests {

    /**
     * Проверка что если в ответе один json объект, то смапится без ошибки
     */
    @Test
    public void testCommonEntityMapping() throws IOException {
/*
        String text = new String(Files.readAllBytes(Paths.get("src/test/resources/neo4jResponse1.json")), StandardCharsets.UTF_8);
        System.out.println(text);

        JsonObject neo4jJson = new JsonObject(text);
        JsonArray res = Neo4jMapping.getCommonEntities(neo4jJson);

        assertEquals(res.size(), 1);
        */
    }

    /**
     * Проверка, что если пустой ответ, что смапится без ошибки
     */
    @Test
    public void testEmptyMapping() throws IOException {

        /*
        String text = "{\"data\":[],\"columns\":[\"e\"]}";

        JsonObject neo4jJson = new JsonObject(text);
        JsonArray res = Neo4jMapping.getCommonEntities(neo4jJson);
        assertEquals(res.size(), 0);
        */
    }


    /**
     * Проверка, что если в ответе несколько объектов, то смапится
     * @throws IOException
     */
    @Test
    public void testEmployeeListMapping() throws IOException {
/*
        AsyncResult<Message<Object>> res = TestUtil.prepareResultFromFile("src/test/resources/neo4jResponseList.json");
        JsonArray employees =  Neo4jEmployeeMapping.mapEmployees(res);
        assertEquals(2, employees.size());
        */
    }


    @Test
    public void testLegacyRequest() throws IOException {
        /*
        JsonObject statement1 = Neo4jRequestMapping.mapLegacyRequest("CREATE (n) RETURN id(n)", new JsonObject());

        JsonObject expect = new JsonObject()
                .put("query", "CREATE (n) RETURN id(n)")
                .put("params",  new JsonObject());
        assertTrue(statement1.equals(expect));

        expect.put("foo", "foo");
        assertFalse(statement1.equals(expect));
        */
    }


    @Test
    public void testTransactionalRequests() throws IOException {
        /*
        JsonObject st1 = Neo4jRequestMapping.mapTransactionalRequest("CREATE (n) RETURN id(n)", new JsonObject());
        JsonObject st2 = Neo4jRequestMapping.mapTransactionalRequest("CREATE (n2) RETURN id(n2)", new JsonObject()
                .put("param1", "val1")
        );
        JsonArray statements = new JsonArray()
                .add(st1)
                .add(st2);
        JsonObject fullRequest = Neo4jRequestMapping.mapTransactionalRequests(statements);


        JsonObject expect = new JsonObject()
                .put("statements", statements);
        assertTrue(fullRequest.equals(expect));
        */
    }

    @Test
    public void testTransactionalResponse() throws IOException {
        /*
        AsyncResult<Message<Object>> response = TestUtil.prepareResultFromFile(
                "src/test/resources/neo4jTransactionalResponse.json");
        JsonObject res = Neo4jSearchMapping.mapSearchResponse(response);

        assertEquals(2, res.getInteger("totalRecords").longValue());
        assertEquals(2, res.getJsonArray("records").size());
        assertEquals(0, res.getJsonArray("records").getJsonObject(0).getLong("id").longValue());
        */

    }





}
