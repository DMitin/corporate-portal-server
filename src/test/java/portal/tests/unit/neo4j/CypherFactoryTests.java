package portal.tests.unit.neo4j;

import org.junit.Test;
import portal.neo4j.CypherFactory;

import static org.junit.Assert.assertEquals;

public class CypherFactoryTests {
    @Test
    public void editEmployeeById() {
        String query = CypherFactory.editEmployeeById();
        System.out.println(query);
    }





//    @Test
//    public void searchCommon() {
//        String query = CypherFactory.createQuerySearchCommon("LOCATION", "name");
//        assertEquals("CYPHER 2.3 START from=node({from}),to=node({to}) MERGE (from)-[:rel]->(to)", query );
//
//    }

    @Test
    public void createCommonMerge() {
        String query = CypherFactory.createQueryAddCommonMerge("Tag");
        assertEquals("CYPHER 2.3 MERGE (c:Tag {name:{name}}) RETURN (c)", query);
    }

    @Test
    public void createRelationship() {
        String query = CypherFactory.createQueryAddRelationship("rel");
        //System.out.println(query);
        assertEquals("CYPHER 2.3 START from=node({from}),to=node({to}) MERGE (from)-[r:rel]->(to) RETURN (r)", query);
    }
}
