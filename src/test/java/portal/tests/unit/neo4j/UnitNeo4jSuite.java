package portal.tests.unit.neo4j;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    CypherFactoryTests.class,
    MappingTests.class
})
public class UnitNeo4jSuite {
}
