package portal.tests.unit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import portal.tests.unit.neo4j.CypherFactoryTests;
import portal.tests.unit.neo4j.MappingTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    CypherFactoryTests.class,
    MappingTests.class
})
public class UnitSuite {
}
