package portal.tests.integration.pojo;

import java.util.Arrays;

/**
 * Created by Denis Mitin on 17.05.2016.
 */
public class SearchResponse {
    public SearchGroup[] searchResult;
    public Integer totalRecords;
    public Integer pageSize;

    public Long getFullSize() {
        return Arrays.stream(searchResult)
                .map(g->g.items)
                .count();
    }
}
