package portal.tests.integration.pojo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import portal.pojo.employee.Employee;
import portal.pojo.employee.EmployeeSmall;

import java.io.Serializable;

/**
 * Created by Denis Mitin on 17.05.2016.
 */
public class EmployeeTest extends Employee implements Serializable {

    public boolean equals(Object obj) {
        boolean eq = EqualsBuilder.reflectionEquals(this, obj);
        return eq;
    }

    public void updateDataValues(String addedValue) {
        this.firstName = this.firstName + addedValue + "1";
        this.lastName = this.lastName + addedValue + "2";
        this.middleName = this.middleName + addedValue + "3";
        this.email = this.email + addedValue + "4";
        this.skype = this.skype + addedValue + "5";
        //this.department = this.department + addedValue;

        this.social.twitter = this.social.twitter + addedValue + "6";
        this.social.facebook = this.social.facebook + addedValue + "7";
        this.social.googlePlus = this.social.googlePlus + addedValue + "8";
        this.social.linkedin = this.social.linkedin + addedValue + "9";
        this.social.instagram = this.social.instagram + addedValue + "10";
        this.social.vk = this.social.vk + addedValue + "11";
    }

    public EmployeeSmall getEmployeeSmall() {
        EmployeeSmall employeeSmall = new EmployeeSmall();

        employeeSmall.id = id;
        employeeSmall.status = status;

        employeeSmall.firstName = firstName;
        employeeSmall.lastName = lastName;
        employeeSmall.middleName = middleName;

        employeeSmall.image = image;

        return employeeSmall;
    }




    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public boolean equals(EmployeeTest employeeTest) {
        return EqualsBuilder.reflectionEquals(this, employeeTest, true);
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void supressId() {
        id = null;
    }
}
