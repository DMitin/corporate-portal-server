package portal.tests.integration.pojo;

/**
 * Created by Denis Mitin on 22.07.2016.
 */
public class AuthResponse {
    String session;
    EmployeeTest employee;

    public AuthResponse(String session, EmployeeTest employee) {
        this.session = session;
        this.employee = employee;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public EmployeeTest getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeTest employee) {
        this.employee = employee;
    }
}
