package portal.tests.integration.utils;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import io.vertx.core.json.JsonObject;
import portal.tests.integration.pojo.EmployeeTest;

import java.io.InputStream;
import java.util.Properties;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class Utils {

    public final static String SESSION_COOKIE_NAME = "vertx-web.session";



    public static Properties loadProps(String fileName) throws Exception {
        InputStream propsStream = Utils.class.getClassLoader().getResourceAsStream(fileName);
        Properties props = new Properties();
        props.load(propsStream);
        propsStream.close();
        return props;
    }



    public static JsonObject getBody(Response response) {
        return new JsonObject(getBodyStr(response));
    }

    public static String getBodyStr(Response response) {
        ResponseBody body = response.getBody(); // получаем тело запорса
        return body.asString(); // преобразуем его в строку

    }



    public static JsonObject getDefaultCredentials() {
        return new JsonObject()
                .put("ldapName", "user")
                .put("password", "pswrd");
    }

    public static JsonObject getDefaultHRCredentials() {
        return new JsonObject()
                .put("ldapName", "userHR")
                .put("password", "pswrd");
    }

    public static JsonObject getDefaultCredentials(String suffix) {
        return new JsonObject()
                .put("ldapName", "user" + suffix)
                .put("password", "pswrd" + suffix);
    }

    public static void validateNewEmployee(EmployeeTest employee) {
        employee.supressId();
        EmployeeTest expected = new EmployeeTest();
        expected.status = "new";
        assertEquals(expected, employee);
    }


    public static byte[] getFileBytes(String url) {
        Response fileResp = given()
                .get(url);
        fileResp.then()
                .statusCode(200);
        return  fileResp.getBody().asByteArray();
    }
}
