package portal.tests.integration.utils.neo4jrest;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.empty;

/**
 * Created by demi0416 on 02.09.2016.
 */
public class Neo4jCommon {

    public static Long addNode(String label, String name) {

        String departmentQuery = "MERGE (d:" + label + " {name: {name}}) RETURN (d)";
        JsonObject departmentProps = new JsonObject()
                .put("name", name);

        JsonObject req = Neo4jCommon.mapTransactionalRequests(
                Neo4jCommon.mapTransactionalRequest(departmentQuery, departmentProps)
        );

        System.out.println("Create: " + label + "with name " + name);
        Response departmentResponse = Neo4jCommon.runStatement(req);
        Long nodeId = from(departmentResponse.asString()).getLong("results[0].data[0].meta[0].id");
        return nodeId;
    }

    public static JsonObject mapTransactionalRequest(String cypherQuery) {
        return new JsonObject()
                .put("statement", cypherQuery);
    }


    public static JsonObject mapTransactionalRequest(String cypherQuery, JsonObject params) {
        return new JsonObject()
                .put("statement", cypherQuery)
                .put("parameters", params);
    }

    public static JsonObject mapTransactionalRequests(JsonObject... statements) {
        ArrayList<JsonObject> objectList = new ArrayList<JsonObject>(Arrays.asList(statements));
        JsonArray sts = new JsonArray(objectList);
        JsonObject root = new JsonObject()
                .put("statements", sts);
        return root;
    }

    public static Response runStatement(JsonObject req) {

        String userName = "neo4j";
        String pass = "secr8t";
        //String host = "localhost";
        int port = 7474;


        String authStr = userName + ":" + pass;
        Base64.Encoder e = Base64.getEncoder();
        String encoded = e.encodeToString(authStr.getBytes());


        Response commonResp = given()
                .port(port)
                .body(req.toString())
                .header("Authorization", "Basic " + encoded)
                .header("Content-Type", "application/json;")
                .post("/db/data/transaction/commit");
        commonResp.then()
                .statusCode(200)
                .body("errors", empty());
        return  commonResp;
    }
}
