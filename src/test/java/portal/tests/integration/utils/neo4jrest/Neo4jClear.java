package portal.tests.integration.utils.neo4jrest;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;

/**
 * Created by Denis Mitin on 29.07.2016.
 */
public class Neo4jClear {

    public static void clearNeo4j() {

        String query = "MATCH (n) "
                + "OPTIONAL MATCH (n)-[r]-() "
                + "DELETE n,r";
        JsonObject req = Neo4jCommon.mapTransactionalRequests(
                Neo4jCommon.mapTransactionalRequest(query)
        );

        System.out.println("Clean neo4j: " + req);
        Neo4jCommon.runStatement(req);
        System.out.println(" end clean neo4j");
    }

}
