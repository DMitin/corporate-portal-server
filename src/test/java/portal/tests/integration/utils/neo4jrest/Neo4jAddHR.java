package portal.tests.integration.utils.neo4jrest;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonObject;
import portal.enums.Department;
import portal.enums.EntityEnum;
import portal.enums.RelationshipEnum;
import portal.neo4j.bolt.Neo4jUser;
import portal.pojo.Relationship;
import portal.pojo.neo4j.Neo4jEmployeeInterface;
import portal.pojo.neo4j.Neo4jEmployeeJson;
import portal.tests.integration.api.Auth;
import portal.tests.integration.pojo.AuthResponse;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.Utils;

import static com.jayway.restassured.path.json.JsonPath.from;


/**
 * Created by demi0416 on 02.09.2016.
 */
public class Neo4jAddHR {

    /**
     * Так как добавить department может только HR, а HRом становится только пользователь в HR department,
     * то для тестов создаём департамент и связываем с пользователем прямым запорсом в базу
     */
    public static Long addHR () {

        //AuthResponse authObject = Auth.loginNew(Utils.getDefaultHRCredentials());
        //Auth.logout();

        String ldapName = Utils.getDefaultHRCredentials().getString("ldapName");
        Neo4jEmployeeInterface neo4jEmployee = new Neo4jEmployeeJson(ldapName);
        JsonObject newUser = neo4jEmployee.getFlatData();
        newUser.put("image", "");

        JsonObject newUserProps = new JsonObject()
                .put("props", newUser);
        JsonObject reqCreateUser = Neo4jCommon.mapTransactionalRequests(
                Neo4jCommon.mapTransactionalRequest(Neo4jUser.addUserQuery, newUserProps)
        );

        System.out.println(reqCreateUser.encodePrettily());
        Response createUserResponse = Neo4jCommon.runStatement(reqCreateUser);
        Long employeeId = from(createUserResponse.asString()).getLong("results[0].data[0].meta[0].id");


        Long departmentId = Neo4jCommon.addNode( EntityEnum.Department, Department.HR);



        String addRelationhipEmployeeDepartment = "START from=node(" + employeeId + "),to=node("+ departmentId +") " +
                "MERGE (from)-[r:"+ RelationshipEnum.DEPARTMENT +"]->(to) RETURN (r)";
        JsonObject reqAddRelationhip = Neo4jCommon.mapTransactionalRequests(
                Neo4jCommon.mapTransactionalRequest(addRelationhipEmployeeDepartment)
        );


        System.out.println("Create Emplooyee --> Department Relationship ");
        System.out.println(reqAddRelationhip.encodePrettily());
        Response relResponse = Neo4jCommon.runStatement(reqAddRelationhip);
        System.out.println(relResponse.asString());
        System.out.println(" end Create Emplooyee --> Department Relationship " + departmentId);

        return departmentId;

    }
}
