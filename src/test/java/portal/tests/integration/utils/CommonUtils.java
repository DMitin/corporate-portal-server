package portal.tests.integration.utils;

import portal.enums.Department;
import portal.pojo.Entity;
import portal.tests.integration.pojo.EmployeeTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by demi0416 on 19.09.2016.
 */
public class CommonUtils {
    public static boolean isEmployeeHR(EmployeeTest employee) {
        int count = 0;
        for (Entity d: employee.departments) {
            if (d.name.equals(Department.HR)) {
                count ++;
            }
        }

        return count == 1;
    }
}
