package portal.tests.integration;

import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import portal.tests.integration.api.Auth;
import portal.tests.integration.api.Search;
import portal.tests.integration.api.User;
import portal.tests.integration.pojo.AuthResponse;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.pojo.SearchResponse;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


/**
 * Created by IPermyakova on 28.04.2016.
 */
public class SearchTests {

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }


    @Test
    public void searcTest() {
        JsonObject authCredentials = new JsonObject()
                .put("ldapName", "user1")
                .put("password", "pswrd1");

        AuthResponse authResponse = Auth.loginNew(authCredentials);
        EmployeeTest employee = authResponse.getEmployee();
        employee.firstName = "testFirstName";
        employee.lastName = "testLastName";
        EmployeeTest saved = User.save(employee);


        JsonObject reqSearchBody = new JsonObject()
                .put("searchString", "First");
        SearchResponse searchResponse = Search.search(reqSearchBody);
        EmployeeTest found = searchResponse.searchResult[0].items[0];
        assertEquals(saved, found);

        reqSearchBody = new JsonObject()
                .put("searchString", "last");
        searchResponse = Search.search(reqSearchBody);
        found = searchResponse.searchResult[0].items[0];
        assertEquals(saved, found);
    }


    /**
     * Проверка что возвращается правильное значение totalRecords
     */
    @Test
    public void testTotalRecords() {
        initEmployees();

        JsonObject reqSearchBodyT = new JsonObject()
                .put("searchString", "t");  // totalRecords зависит только от строки поиска
        SearchResponse resultT = User.search(reqSearchBodyT);
        assertEquals(4, resultT.totalRecords.intValue());


        JsonObject reqSearchBodyB = new JsonObject()
                .put("searchString", "b");
        SearchResponse resultB = User.search(reqSearchBodyB);
        assertEquals(1, resultB.totalRecords.intValue());
    }


    /**
     * Проверка что возвращается правильное значение totalRecords
     */
    @Test
    public void testPageSize() {
        initEmployees();

        JsonObject reqSearchBody3 = new JsonObject()
                .put("searchString", "t")
                .put("pageSize", 3);
        SearchResponse result3 = User.search(reqSearchBody3);
        assertEquals(3L, result3.getFullSize().longValue());


        JsonObject reqSearchBody2 = new JsonObject()
                .put("searchString", "t")
                .put("pageSize", 2);
        SearchResponse result2 = User.search(reqSearchBody2);
        assertEquals(2L, result2.getFullSize().longValue());
    }


    public void initEmployees() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("user1", "ta");
        map.put("user2", "bt");
        map.put("user3", "tc");
        map.put("user4", "dt");
        map.put("user5", "ff");

        JsonObject fio = new JsonObject()
                .put("firstName", "fName")
                .put("middleName", "middleName");

        map.forEach((k, v) -> {
            JsonObject authCredentials = new JsonObject()
                    .put("ldapName", k)
                    .put("password", "pswrd1");

            EmployeeTest employee = Auth.login(authCredentials).getEmployee();
            employee.firstName = "fName";
            employee.lastName = v;
            User.save(employee);
        });
    }


}
