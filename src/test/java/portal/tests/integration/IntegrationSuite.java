package portal.tests.integration;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    AsyncVertxTest.class,
    AuthTests.class,
    CommonTests.class,
    HRTests.class,
    DateTests.class,
    SearchTests.class,
    UpdateEmployeeTests.class
})
public class IntegrationSuite {
}
