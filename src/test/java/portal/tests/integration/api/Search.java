package portal.tests.integration.api;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonObject;
import portal.tests.integration.pojo.SearchResponse;
import portal.tests.integration.utils.Utils;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Denis Mitin on 25.07.2016.
 */
public class Search {
    public static SearchResponse search(JsonObject userData) {
        Response searchResp = searchToResponse(userData);
        searchResp.then()
                .statusCode(200);

        SearchResponse searchResponse = searchResp.as(SearchResponse.class);
        return searchResponse;
    }

    public static Response searchToResponse(JsonObject reqSearchBody) {
        return given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .body(reqSearchBody.toString())
                .post("rest/user/search");
    }
}
