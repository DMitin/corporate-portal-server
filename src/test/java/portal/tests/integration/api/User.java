package portal.tests.integration.api;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonObject;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.pojo.SearchResponse;
import portal.tests.integration.utils.Utils;

import java.io.File;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Denis Mitin on 17.05.2016.
 */
public class User {



    public static EmployeeTest uploadImage(File file) {
        Response uploadResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .multiPart(file)
                .contentType("multipart/form-data")
                .post("/rest/user/loadImage");

        uploadResp.then()
                .statusCode(200);

        EmployeeTest employeeData = uploadResp.as(EmployeeTest.class);
        return employeeData;
    }

    public static EmployeeTest getById(Long id) {
        Response uploadResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .get("/rest/user/" + id);

        uploadResp.then()
                .statusCode(200);

        EmployeeTest employeeData = uploadResp.as(EmployeeTest.class);
        return employeeData;
    }


    public static EmployeeTest save(EmployeeTest employee) {
        Response putResp = saveRestImpl(employee);
        long time = putResp.then()
                .statusCode(200)
                .extract().response().time();
        System.out.println("save employee: " + time);

        EmployeeTest employeeData = putResp.as(EmployeeTest.class);
        return employeeData;
    }

    public static Response saveRestImpl(EmployeeTest employee) {
        return given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .body(employee)
                .put("rest/user/");
    }


    public static SearchResponse search(JsonObject searchData) {
        Response searchResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .body(searchData.toString())
                .post("rest/user/search");

        searchResp.then()
                .statusCode(200);

        SearchResponse searchResult = searchResp.as(SearchResponse.class);
        return searchResult;
    }
}
