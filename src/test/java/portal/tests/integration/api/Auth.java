package portal.tests.integration.api;

import com.jayway.restassured.response.Response;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.SerializationUtils;
import portal.tests.integration.pojo.AuthResponse;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.Utils;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Denis Mitin on 17.05.2016.
 */
public class Auth {

    public static String currentSession;
    public static final String LOGOUT_SUCCESS_MESSAGE = "Пользователь удачно вышел с портала";



    public static AuthResponse loginNew(JsonObject credentials) {
        Response authResp = given()
                .body(credentials.toString())
                .post("rest/auth/login");
        authResp.then()
                .statusCode(200);

        EmployeeTest employeeData = authResp.as(EmployeeTest.class);
        assertNotNull(employeeData.id);
        EmployeeTest cloned = SerializationUtils.clone(employeeData);
        Utils.validateNewEmployee(cloned);

        String session = authResp.cookie(Utils.SESSION_COOKIE_NAME);
        Auth.currentSession = session;

        return new AuthResponse(session, employeeData);
    }

    public static AuthResponse login(JsonObject credentials) {
        Response authResp = given()
                .body(credentials.toString())
                .post("rest/auth/login");
        authResp.then()
                .statusCode(200);

        EmployeeTest employeeData = authResp.as(EmployeeTest.class);
        assertNotNull(employeeData.id);
        String session = authResp.cookie(Utils.SESSION_COOKIE_NAME);
        Auth.currentSession = session;

        return new AuthResponse(session, employeeData);
    }

    public static void logout() {
        Response logoutResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .post("rest/auth/logout");
        logoutResp.then()
                .statusCode(200);

        Auth.currentSession = null;

        String logoutMessage = Utils.getBodyStr(logoutResp);
        assertEquals(Auth.LOGOUT_SUCCESS_MESSAGE, logoutMessage);
    }
}
