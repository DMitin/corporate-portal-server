package portal.tests.integration.api;

import com.jayway.restassured.response.Response;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.tests.integration.utils.Utils;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Denis Mitin on 08.08.2016.
 */
public class Common {

    public static Entity object(String label, String name) {

        Entity entity = new Entity(label, name);
       return Common.object(entity);
    }

    public static Entity object(Entity entity) {

        Response putResp = objectRestImpl(entity);
        putResp.then()
                .statusCode(200);

        Entity entitySaved = putResp.as(Entity.class);
        assertNotNull(entitySaved.id);
        return entitySaved;
    }

    public static Response objectRestImpl(Entity entity) {

        return given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .body(entity)
                .post("rest/common/object");
    }

    public static Relationship relationship(Long from, Long to, String name) {

        Relationship relationship = new Relationship();
        relationship.from = from;
        relationship.to = to;
        relationship.name = name;

        return  Common.relationship(relationship);
    }

    public static Relationship relationship(Relationship relationship) {

        Response putResp = relationshipRestImpl(relationship);
        putResp.then()
                .statusCode(200);

        Relationship relSaved = putResp.as(Relationship.class);
        assertNotNull(relSaved.id);
        return relSaved;
    }

    public static Response relationshipRestImpl(Relationship relationship) {

        Response postResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .body(relationship)
                .post("rest/common/relationship");
        return postResp;
    }
}
