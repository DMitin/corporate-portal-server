package portal.tests.integration.api;

import com.jayway.restassured.response.Response;
import portal.tests.integration.pojo.MonthBirthdays;
import portal.tests.integration.utils.Utils;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Denis Mitin on 08.06.2016.
 */
public class Date {

    public static MonthBirthdays getBirthdaysOfMonth(int monthNumber) {
        Response searchResp = given()
                .cookie(Utils.SESSION_COOKIE_NAME, Auth.currentSession)
                .contentType("application/json")
                .get("rest/date/month/" + monthNumber);

        searchResp.then()
                .statusCode(200);

        searchResp.prettyPrint();

        MonthBirthdays searchResult = searchResp.as(MonthBirthdays.class);
        return searchResult;
    }
}
