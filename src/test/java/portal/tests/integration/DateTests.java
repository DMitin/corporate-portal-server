package portal.tests.integration;

import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import portal.pojo.DayBirthdays;
import portal.pojo.employee.EmployeeSmall;
import portal.tests.integration.api.Auth;
import portal.tests.integration.api.Date;
import portal.tests.integration.api.User;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.pojo.MonthBirthdays;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;
import portal.tests.integration.utils.Utils;

import static org.junit.Assert.assertEquals;

/**
 * Created by Denis Mitin on 08.06.2016.
 */
public class DateTests {
    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }

    /**
     * получить дни рождения у пользователя, у которого не заполнено поле birthday
     */
    @Test
    public void getBirthdayNotAssigned() {

        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("testDate");
        User.save(employee);

        MonthBirthdays monthBirthdays = Date.getBirthdaysOfMonth(1);
        assertEquals(0, monthBirthdays.birthdayses.size());
    }

    @Test
    public void getBirthdayAssigned() {

        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("testDate");
        employee.birthday = "02.01";
        EmployeeTest saved = User.save(employee);

        MonthBirthdays monthBirthdays = Date.getBirthdaysOfMonth(1);
        assertEquals(1, monthBirthdays.birthdayses.size());

        DayBirthdays dayBirthdays = monthBirthdays.birthdayses.get(0);
        assertEquals(1, dayBirthdays.employees.size());
        assertEquals(2, dayBirthdays.day);
        EmployeeSmall employeeSmall = saved.getEmployeeSmall();
        assertEquals(employeeSmall, dayBirthdays.employees.get(0));
    }

    @Test
    public void setNotValidBirthday() {

        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("testDate");
        employee.birthday = "zzz";
        Response response = User.saveRestImpl(employee);
        response.then()
                .statusCode(405);
    }

    @Test
    public void setBirthdayRemoveBirthday() {

        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("testDate");
        employee.birthday = "02.01";
        EmployeeTest saved = User.save(employee);

        saved.birthday = "";
        User.save(saved);

        MonthBirthdays monthBirthdays = Date.getBirthdaysOfMonth(1);
        assertEquals(0, monthBirthdays.birthdayses.size());
    }

    @Test
    public void manyBirthdaysInMonth() {

        EmployeeTest employee0 = Auth.login(Utils.getDefaultCredentials("0")).getEmployee();
        employee0.updateDataValues("testDate");
        employee0.birthday = "02.01";
        EmployeeTest e1 = User.save(employee0);

        EmployeeTest employee1 = Auth.login(Utils.getDefaultCredentials("1")).getEmployee();
        employee1.updateDataValues("testDate");
        employee1.birthday = "03.01";
        EmployeeTest e2 = User.save(employee1);

        EmployeeTest employee2 = Auth.login(Utils.getDefaultCredentials("2")).getEmployee();
        employee2.updateDataValues("testDate");
        employee2.birthday = "04.01";
        EmployeeTest e3 = User.save(employee2);

        MonthBirthdays monthBirthdays = Date.getBirthdaysOfMonth(1);
        assertEquals(3, monthBirthdays.birthdayses.size());

        DayBirthdays b02 = monthBirthdays.birthdayses.get(0);
        assertEquals(2, b02.day);
        assertEquals(1, b02.employees.size());
        assertEquals(e1.getEmployeeSmall(), b02.employees.get(0));

        DayBirthdays b03 = monthBirthdays.birthdayses.get(1);
        assertEquals(3, b03.day);
        assertEquals(1, b03.employees.size());
        assertEquals(e2.getEmployeeSmall(), b03.employees.get(0));

        DayBirthdays b04 = monthBirthdays.birthdayses.get(2);
        assertEquals(4, b04.day);
        assertEquals(1, b04.employees.size());
        assertEquals(e3.getEmployeeSmall(), b04.employees.get(0));
    }

    @Test
    public void setBirthdayAndUpdate() {

        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("testDate");
        employee.birthday = "01.01";
        EmployeeTest saved = User.save(employee);

        saved.birthday = "02.02";
        User.save(saved);

        MonthBirthdays monthBirthdays01 = Date.getBirthdaysOfMonth(1);
        assertEquals(0, monthBirthdays01.birthdayses.size());

        MonthBirthdays monthBirthdays02 = Date.getBirthdaysOfMonth(2);
        assertEquals(1, monthBirthdays02.birthdayses.size());
    }
}
