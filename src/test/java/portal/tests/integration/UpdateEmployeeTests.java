package portal.tests.integration;

import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import portal.enums.EmployeeStatuses;
import portal.tests.integration.api.Auth;
import portal.tests.integration.api.User;
import portal.tests.integration.pojo.AuthResponse;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.neo4jrest.Neo4jAddHR;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;
import portal.tests.integration.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UpdateEmployeeTests {

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }


    @Test
    /*
        1. Данные которые установили сотруднику сохраняются и возвращаются с сервера
        2. Так как были установлены firstName и lastName то меняется статус new->valid
     */
    public void updateUserDataAndCheckStatus() {

        AuthResponse authObject = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = authObject.getEmployee();
        employee.updateDataValues(employee.id.toString());

        // запрос на изменение данных пользователя
        EmployeeTest employeeSave = User.save(employee);
        System.out.println("Измененные данные пользователя " + employeeSave.toString());

        employee.status = "valid";
        assertEquals(employeeSave, employee);
    }

    @Test
    public void updateUserDataWithWrongId() {

        AuthResponse authObject = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = authObject.getEmployee();
        employee.updateDataValues(employee.id.toString());
        employee.id += 1;

        Response response = User.saveRestImpl(employee);
        response.then()
                .statusCode(403);
    }

    @Test
    public void updateUserDataAndStatusAndGet() {

        AuthResponse authObject = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = authObject.getEmployee();
        employee.updateDataValues(employee.id.toString());

        User.save(employee);
        Auth.logout();

        EmployeeTest employeeGet = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        employee.status = "valid";
        assertEquals(employee, employeeGet);
    }




    /**
     * 1. Авторизоваться (пустые данные пользователя)
     * 2. сохранить пустые данные
     * 3. получить статус 405 (не прощла валидация на сервере)
     */
    @Test
    public void updateUserValidation() {

        AuthResponse authResponse = Auth.loginNew(Utils.getDefaultCredentials());
        Response putRespId = User.saveRestImpl(authResponse.getEmployee());
        putRespId.then()
                .statusCode(405);
    }

    /**
     * 1. Авторизоваться
     * 2. заполнить firstName, lastName
     * 3. изменить id
     * 4. сохранить данные
     * 5. получить 403 статус (не соответствие id)
     */
    @Test
    public void updateUserBadId() {

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();
        employee.updateDataValues("zzz");
        employee.id = employee.id + 1;

        Response putRespId = User.saveRestImpl(employee);
        putRespId.then()
                .statusCode(403);
    }

    @Test
    public void uploadImageAndCheckBytes() throws IOException {

        File file = new File("fl.png");
        byte[] data = Files.readAllBytes(file.toPath());
        Auth.loginNew(Utils.getDefaultCredentials());
        String image = User.uploadImage(file).image;
        byte[] userImageBytes = Utils.getFileBytes(image);
        assertTrue(Arrays.equals(data, userImageBytes));

    }

    @Test
    public void uploadImageAndGetImagePath() {

        File file = new File("fl.png");

        Auth.loginNew(Utils.getDefaultCredentials());
        String imagePath = User.uploadImage(file).image;
        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        assertEquals(imagePath, employee.image);
    }

    @Test
    /*
        Загрузка фотографии (без задания firstName и lastName) не меняет статуса new->valid
     */
    public void uploadImageAndCheckStatus() {

        File file = new File("fl.png");
        Auth.loginNew(Utils.getDefaultCredentials());
        User.uploadImage(file);
        EmployeeTest employee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        assertEquals(EmployeeStatuses.NEW, employee.status);
    }

    @Test
    /*
        Установка фотографии пользователю со статусом valid, не меняет статус.
     */
    public void uploadImageAndCheckStatusForValid() {

        AuthResponse authObject = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = authObject.getEmployee();
        employee.updateDataValues(employee.id.toString());

        EmployeeTest employeeSave = User.save(employee);
        assertEquals(EmployeeStatuses.VALID, employeeSave.status);

        File file = new File("fl.png");
        EmployeeTest employeeWithImage = User.uploadImage(file);
        assertEquals(EmployeeStatuses.VALID, employeeWithImage.status);
    }

    @Test
    public void cantUpdateUserDataImage() {

        AuthResponse authObject = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = authObject.getEmployee();
        employee.updateDataValues(employee.id.toString());
        employee.image = "zzz";

        // запрос на изменение данных пользователя
        EmployeeTest employeeSave = User.save(employee);
        assertEquals("", employeeSave.image);
    }

    @Test
    public void notHrCantUpdateDataOfOtherUser() {

        EmployeeTest ordinaryEmployee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        Auth.login(Utils.getDefaultCredentials("2")).getEmployee(); // updating portal.tests.integration.api.Auth.currentSession

        ordinaryEmployee.updateDataValues(ordinaryEmployee.id.toString());

        // запрос на изменение данных пользователя
        Response updateResponse = User.saveRestImpl(ordinaryEmployee);
        updateResponse.then()
                .statusCode(403);
    }



}
