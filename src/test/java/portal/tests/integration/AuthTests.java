package portal.tests.integration;

import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import portal.tests.integration.api.Auth;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;

import static org.junit.Assert.assertEquals;

/**
 * Created by Denis Mitin on 20.05.2016.
 */
public class AuthTests {

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }

    @Test
    public void loginLogout() {
        JsonObject authCredentials = new JsonObject()
                .put("ldapName", "user1")
                .put("password", "pswrd1");

        Auth.loginNew(authCredentials);
        Auth.logout();
    }

    @Test
    public void loginLogoutLogin() {
        JsonObject authCredentials = new JsonObject()
                .put("ldapName", "user1")
                .put("password", "pswrd1");

        EmployeeTest employee = Auth.loginNew(authCredentials).getEmployee();
        Auth.logout();
        EmployeeTest employee2 = Auth.loginNew(authCredentials).getEmployee();
        assertEquals(employee, employee2);
    }
}
