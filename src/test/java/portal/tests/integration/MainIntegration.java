package portal.tests.integration;


import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import io.vertx.core.json.JsonObject;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Denis Mitin on 13.04.2016.
 */



public class MainIntegration {

    final static String SESSION_COOKIE_NAME = "vertx-web.session";

    public static void main(String[] args) {
        /*
            1 часть. Нужно авторизоваться на портале и получить cookie
            чтоб потом его использовать для запросов
         */

        /*
            Тело запроса авторизиции.
         */
        JsonObject reqBody = new JsonObject()
                .put("ldapName", "user1")
                .put("password", "pswrd1");

        // создаём запрос авторизации
        Response authResp  = given()
                .body(reqBody.toString()) // тело запросма (json)
                .post("rest/auth/loginNew"); // тип запроса (post) и на какой адрес посылаем

        // получаем значение cookie с заданным именем, это и есть сессия которую будем использовать для запросов api
        String session = authResp.cookie(SESSION_COOKIE_NAME);
        System.out.println(session);

        /*
            Работаем с телом запроса
         */
        ResponseBody authBody = authResp.getBody(); // получаем тело запорса
        String bodyStr = authBody.asString(); // преобразуем его в строку
        JsonObject authBodyJson = new JsonObject(bodyStr); // JsonObject - json объект запорса
        long userId = authBodyJson.getLong("id"); // получаем id
        /*
            просто выводим в консоль (на самом деле должны быть всякие проверки)
         */
        System.out.println("Авторизация: " + authBodyJson.encodePrettily());


        /*
            2 Запрос, получение текущего пользователя
         */
        Response currentUserResp  = given()
                .cookie(SESSION_COOKIE_NAME, session)
                .body(reqBody.toString())
                .get("rest/user/" + userId);

        ResponseBody currentUserBody = currentUserResp.getBody();
        String currentUserBodyStr = currentUserBody.asString();
        JsonObject currentUserBodyJson = new JsonObject(currentUserBodyStr);
        System.out.println("Данные пользователя [" + userId + "] : "   + currentUserBodyJson.encodePrettily());

    }
}
