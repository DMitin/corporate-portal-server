package portal.tests.integration;

import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import portal.enums.Department;
import portal.enums.EntityEnum;
import portal.enums.RelationshipEnum;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.tests.integration.api.Auth;
import portal.tests.integration.api.Common;
import portal.tests.integration.api.User;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.neo4jrest.Neo4jAddHR;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;
import portal.tests.integration.utils.Utils;
import portal.tests.integration.utils.neo4jrest.Neo4jCommon;

import static org.junit.Assert.*;

/**
 * Created by Denis Mitin on 08.08.2016.
 */
public class CommonTests {

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }




    /*
    @Test
    public void createDuplicateEntity() {

        Auth.loginNew(Utils.getDefaultCredentials());

        Entity entity = new Entity("Department", "HR");
        Entity saved = Common.object(entity);

        Entity saved2 = Common.object(entity);
        assertEquals(saved.id, saved2.id);
    }
    */

    @Test
    public void createRelationhip() {

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();

        Entity tag = new Entity("Tag", "Neo4j");
        Entity tagSaved = Common.object(tag);

        Relationship relSaved =  Common.relationship(employee.id, tagSaved.id, "Skill");
    }

    @Test
    public void createRelationhipData() {

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();

        Entity tagSaved = Common.object("Tag", "Neo4j");

        Relationship relSaved =  Common.relationship(employee.id, tagSaved.id, "Skill");
        assertEquals(relSaved.from, employee.id);
        assertEquals(relSaved.to, tagSaved.id);
    }

    @Test
    public void createRelationhipDuplicate() {

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();

        Entity tagSaved = Common.object("Tag", "Neo4j");

        Relationship relationship = new Relationship();
        relationship.from = employee.id;
        relationship.to = tagSaved.id;
        relationship.name = "Skill";

        Relationship relSaved =  Common.relationship(relationship);
        Relationship relSaved2 =  Common.relationship(relationship);

        assertEquals(relSaved.id, relSaved2.id);
    }


    @Test
    public void addRelationhipNodesNotInCash() {

        Long fakeNodeId1 = Neo4jCommon.addNode("FakeNode", "fakeNode1");
        Long fakeNodeId2 = Neo4jCommon.addNode("FakeNode", "fakeNodeName");

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();
        Relationship relSaved =  Common.relationship(fakeNodeId1, fakeNodeId2, "Fakerelationship");
    }

}
