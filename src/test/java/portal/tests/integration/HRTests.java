package portal.tests.integration;

import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import portal.enums.Department;
import portal.enums.EntityEnum;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.tests.integration.api.Auth;
import portal.tests.integration.api.Common;
import portal.tests.integration.api.User;
import portal.tests.integration.pojo.AuthResponse;
import portal.tests.integration.pojo.EmployeeTest;
import portal.tests.integration.utils.CommonUtils;
import portal.tests.integration.utils.Utils;
import portal.tests.integration.utils.neo4jrest.Neo4jAddHR;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by demi0416 on 09.09.2016.
 */
public class HRTests {

    /*
    @Test
    public void addEmployeeToHrDepartmentCreateHR() {

        Neo4jAddHR.addHR();

        EmployeeTest ordinaryEmployee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        Auth.login(Utils.getDefaultCredentials("2")).getEmployee(); // updating portal.tests.integration.api.Auth.currentSession

        ordinaryEmployee.updateDataValues(ordinaryEmployee.id.toString());

        // запрос на изменение данных пользователя
        Response updateResponse = User.saveRestImpl(ordinaryEmployee);
        updateResponse.then()
                .statusCode(403);
    }
*/

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }


    @Test
    public void createHrAndCheckItsDepartments() {

        Neo4jAddHR.addHR();

        EmployeeTest employeeHR = Auth.login(Utils.getDefaultHRCredentials()).getEmployee();
        CommonUtils.isEmployeeHR(employeeHR);
    }

    @Test
    public void hrCanUpdateDataOfOtherUser() {

        Neo4jAddHR.addHR();

        EmployeeTest ordinaryEmployee = Auth.login(Utils.getDefaultCredentials()).getEmployee();
        Auth.login(Utils.getDefaultHRCredentials()).getEmployee(); // work as HR

        ordinaryEmployee.updateDataValues(ordinaryEmployee.id.toString());
        EmployeeTest savedEmployee = User.save(ordinaryEmployee);
        EmployeeTest withUpdatedData = User.getById(ordinaryEmployee.id);
        assertTrue(savedEmployee.equals(withUpdatedData));


    }

    @Test
    public void hrCanCreateDepartment() {

        Neo4jAddHR.addHR();

        Auth.login(Utils.getDefaultHRCredentials());
        Entity entity = new Entity("Department", "HR");
        Entity saved = Common.object(entity);
        assertTrue(saved.equalsIgnoreId(entity));
    }

    @Test
    public void notHrCantCreateDepartment() {

        Auth.login(Utils.getDefaultCredentials());
        Entity entity = new Entity("Department", "HR");
        Response savedResponse = Common.objectRestImpl(entity);
        savedResponse
                .then()
                .statusCode(403);
    }

    @Test
    public void hrCanAddEmployeeToDepartment() {

        Long departmentId = Neo4jAddHR.addHR();

        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();
        /*
            используем сессию HR.
            Department тоже попадёт в кэш, проверяем вариант что оба объекта в кэше
          */
        Auth.login(Utils.getDefaultHRCredentials()).getEmployee();

        Relationship relSaved =  Common.relationship(employee.id, departmentId, EntityEnum.Department);
        assertEquals(relSaved.from, employee.id);
        assertEquals(relSaved.to, departmentId);
    }

    @Test
    public void notHrCantAddEmployeeToDepartment() {

        Long departmentId = Neo4jAddHR.addHR();

        /*
            Так как этообычный пользователь, у него при логине не было Departments.
            Тестируем случай, что один параметр в кэше (сотрудник в кэше - department нет)
         */
        EmployeeTest employee = Auth.loginNew(Utils.getDefaultCredentials()).getEmployee();
        Response relSaved =  Common.relationshipRestImpl(new Relationship(
                employee.id, departmentId, EntityEnum.Department));
        assertEquals(401, relSaved.statusCode());
    }

    @Test
    public void newaddedHrCanAddNewDepartment() {

        Long departmentId = Neo4jAddHR.addHR();

        AuthResponse employeeAuth = Auth.loginNew(Utils.getDefaultCredentials());
        EmployeeTest employee = employeeAuth.getEmployee();
        Auth.login(Utils.getDefaultHRCredentials()).getEmployee();
        Relationship relSaved =  Common.relationship(employee.id, departmentId, EntityEnum.Department);

        Auth.currentSession = employeeAuth.getSession(); //начинаем работать от лица обычного сотрудника
        Entity newDepartment = Common.object(EntityEnum.Department, "New Department");
    }
}
