package portal.tests.integration;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;
import portal.tests.integration.utils.Utils;

/**
 * Created by demi0416 on 29.08.2016.
 */

@RunWith(VertxUnitRunner.class)
public class AsyncVertxTest {

    Long id = null;
    Logger logger = LoggerFactory.getLogger("Server");

    @Before
    public void clearNeo4j() {
        Neo4jClear.clearNeo4j();
    }

    /*
        Проверка, что не дублируются пользовали при создании
     */
    @Test
    public void testParalelAuth(TestContext context) {
        final Async async = context.async();

        Handler<HttpClientResponse> handler = response -> {

            response.handler(body -> {
                JsonObject respJson = new JsonObject(body.toString());
                Long responseId = respJson.getLong("id");
                System.out.println("id = " + responseId);
                logger.debug("id = " + responseId);
                if (id == null) {
                    id = responseId;
                } else {
                    context.assertEquals(id, responseId);
                    async.complete();
                }
            });
        };

        Vertx.vertx().createHttpClient().post(8080, "localhost", "/rest/auth/login", handler)
                 .end(Utils.getDefaultCredentials().toString());

        Vertx.vertx().createHttpClient().post(8080, "localhost", "/rest/auth/login", handler)
                .end(Utils.getDefaultCredentials().toString());
    }
}
