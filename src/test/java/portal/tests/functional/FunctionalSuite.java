package portal.tests.functional;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    EmployeeTests.class,
})
public class FunctionalSuite {
}
