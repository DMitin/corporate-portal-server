package portal.tests.functional;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import portal.pojo.employee.EmployeeSmall;
import portal.tests.integration.utils.neo4jrest.Neo4jClear;

import java.util.Properties;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertTrue;
import static portal.tests.integration.utils.Utils.loadProps;

public class EmployeeTests {

    @Before
    public void setUp() throws Exception {
        Neo4jClear.clearNeo4j();
        setUpWebDriverProps();
        setUpSelenideProps();
    }

    private void setUpWebDriverProps() throws Exception {
        Properties webDriverProps = loadProps("webdriver.properties");
        System.setProperty("webdriver.chrome.bin", webDriverProps.getProperty("chrome.bin"));
        System.setProperty("webdriver.firefox.bin", webDriverProps.getProperty("firefox.bin"));
    }

    private void setUpSelenideProps() throws Exception {
        Properties selenideProps = loadProps("selenide.properties");
        System.setProperty("selenide.baseUrl", selenideProps.getProperty("baseUrl"));
    }



    @Test
    public void testNewUserLoginAndCardUpdate() {

        open("/login");

        login("DMitin", "DMitin");

        goToCardEditView();

        EmployeeSmall savedEmployeeData = new EmployeeSmall();
        savedEmployeeData.setLastName("Mitin");
        savedEmployeeData.setFirstName("Denis");
        fillInEmployeeCard(savedEmployeeData);

        EmployeeSmall fetchedEmployeeData = getEmployeeCard();

        assertTrue(
            "Saved employee data is not equal to fetched employee data.\nSaved last name: "
            + savedEmployeeData.getLastName()
            + "\nFetched last name: "
            + fetchedEmployeeData.getLastName()
            + "\n",

            savedEmployeeData.getLastName().equals(fetchedEmployeeData.getLastName())
        );

        assertTrue(
            "Saved employee data is not equal to fetched employee data.\nSaved first name: "
            + savedEmployeeData.getFirstName()
            + "\nFetched first name: "
            + fetchedEmployeeData.getFirstName()
            + "\n",

            savedEmployeeData.getFirstName().equals(fetchedEmployeeData.getFirstName())
        );

    }

    private void login(String login, String password) {
        $(By.id("login-input")).setValue(login);
        $(By.id("password-input")).setValue(password);
        $(By.id("login-button")).click();
    }

    private void goToCardEditView() {
        open("/card");
        $(By.id("edit-anchor")).click();
    }

    private void fillInEmployeeCard(EmployeeSmall employee) {
        $(By.id("lastname-input")).setValue(employee.getLastName());
        $(By.id("firstname-input")).setValue(employee.getFirstName());
        $(By.id("edit-card-save-button")).click();
    }

    private EmployeeSmall getEmployeeCard() {
        open("/card");
        EmployeeSmall employee = new EmployeeSmall();
        String employeeNameString = $(By.cssSelector(".name")).getText();
        String[] employeeNameClauses = employeeNameString.split(" ");
        employee.setLastName(employeeNameClauses[0]);
        employee.setFirstName(employeeNameClauses[1]);
        return employee;
    }

}
