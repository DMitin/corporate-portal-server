package portal.validation;

import io.vertx.core.json.JsonObject;

/**
 * Created by user on 15.05.2016.
 */
public class ValidateEmployee {

    public static boolean validate(JsonObject employee) {
        String firstName = employee.getString("firstName");
        String lastName = employee.getString("lastName");

        return (firstName != null && !firstName.isEmpty())
                && (lastName != null && !lastName.isEmpty());
    }
}
