package portal.validation;

import portal.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;

/**
 * Created by Denis Mitin on 27.07.2016.
 */
public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {

    private DateFormat constraintAnnotation;

    @Override
    public void initialize(DateFormat constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return true;
        } else {
            try {
                Utils.DATE_FORMAT.parse(value);
            } catch (ParseException e) {
                return false;
            }
            return true;
        }

    }
}
