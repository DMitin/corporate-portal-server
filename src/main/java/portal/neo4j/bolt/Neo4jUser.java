package portal.neo4j.bolt;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.neo4j.driver.internal.value.NullValue;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.Values;
import org.neo4j.driver.v1.types.Node;
import portal.enums.RelationshipEnum;
import portal.pojo.neo4j.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Mitin on 31.05.2016.
 */
public class Neo4jUser extends AbstractVerticle {
    public static String addUserQuery = "MERGE ((e:Employee {ldapName: {props}.ldapName})) "
            + "ON CREATE SET e += { props } "
            + "return e";
    String getEmployeeById = "START e=NODE({employeeId}) "
            + "OPTIONAL MATCH (e)-[:BIRTHDATE]->(d)-[:MONTH]->(m) "
            + "OPTIONAL MATCH (e)-[:" + RelationshipEnum.DEPARTMENT + "]->(department) "
            + "RETURN e, d as day, m as month, collect(distinct department) as departments";
    String getEmployeeByLdapName = "MATCH (e:Employee {ldapName: {ldapName}} )"
            + "OPTIONAL MATCH (e)-[:BIRTHDATE]->(d)-[:MONTH]->(m) "
            + "OPTIONAL MATCH (e)-[:" + RelationshipEnum.DEPARTMENT + "]->(department) "
            + "RETURN e, d as day, m as month, collect(distinct department) as departments";
    String editEmployeeById = "START e=NODE({employeeId}) "
            + "SET e += { props } RETURN e";

    String setEmployeeImage = "START e=NODE({employeeId}) "
            +  "SET e.image = { image } RETURN e";
    String deleteEmployeeDate = "START e=NODE({employeeId}) "
            +  "OPTIONAL MATCH (e)-[oldBirthday:BIRTHDATE]-() "
            + "DELETE oldBirthday ";
    String editEmployeeByIdWithDate = "START e=NODE({employeeId}) "
            + "SET e += { props } "
            + "MERGE (d:DAY {number: {day}}) "
            + "MERGE (m:MONTH {number: {month}}) "
            + "MERGE (d)-[:MONTH]->(m) "
            + "MERGE (e)-[:BIRTHDATE]-(d)"
            //"CREATE UNIQUE (e)-[:BIRTHDATE]->(d:DAY {number: {day}})-[:MONTH]->(m:MONTH {number: {month}}) " +
            + "RETURN e, d as day, m as month";

    String searchEmployeeByTxt = "MATCH (e:Employee) "
            + "WHERE e.searchString CONTAINS {searchString} AND NOT e.status  = 'new' "
            + "RETURN e "
            + "ORDER BY e.lastName "
            + "SKIP {skipSize} LIMIT {pageSize} ";

    String searchEmployeeByTxtNumber = "MATCH (e:Employee) "
            + "WHERE e.searchString CONTAINS {searchString}  AND NOT e.status  = 'new' "
            + "RETURN count(e)";


    Logger logger = LoggerFactory.getLogger(Neo4jUser.class.getName());


    @Override
    public void start() {

        EventBus eb = vertx.eventBus();


        eb.consumer("neo4j.user.getById", this::getById);
        eb.consumer("neo4j.user.newUser", this::newUser);
        eb.consumer("neo4j.user.searchByLdap", this::searchByLdap);
        eb.consumer("neo4j.user.editById", this::editById);
        eb.consumer("neo4j.user.setImage", this::setImage);
        eb.consumer("neo4j.user.searchByText", this::searchByText);

    }

    private void getById(Message message) {
        Long employeeId = (Long)message.body();
        logger.debug("neo4j.user.getById:" + employeeId);

        Request req = new Request(getEmployeeById, Values.parameters("employeeId", employeeId) );
        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {
            handleSingleEmployee(cypherResponse, message);
        });
    }

    private void searchByLdap(Message message) {
        String ldapName = (String)message.body();
        logger.debug("searchByLdap: " + ldapName);

        Request req = new Request();
        req.query = getEmployeeByLdapName;
        req.params = Values.parameters("ldapName", ldapName);

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {
            handleSingleEmployee(cypherResponse, message);
        });
    }

    private void searchByText(Message message) {
        JsonObject searchData = (JsonObject) message.body();
        logger.debug("neo4j.user.searchByText:" + searchData);

        Integer pageNumber =  searchData.getInteger("pageNumber");
        Integer pageSize = searchData.getInteger("pageSize");
        String searchString = searchData.getString("searchString");
        Boolean descending = searchData.getBoolean("descending");

        MultiRequest fullRequest = new MultiRequest();
        fullRequest.requests = new ArrayList<>();
        fullRequest.requests.add( new Request(
                searchEmployeeByTxt,
                Values.parameters(
                        "searchString", searchString,
                        "pageSize", pageSize,
                        "skipSize", pageNumber * pageSize
                )
        ));

        fullRequest.requests.add( new Request(searchEmployeeByTxtNumber, Values.parameters(
                "searchString", searchString
        )));

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, fullRequest, cypherResponse -> {
            if (cypherResponse.succeeded()) {
                MultiResult mr = (MultiResult)cypherResponse.result().body();
                List<Record> records = mr.results.get(0).records;
                List<Neo4jEmployeeInterface> employeesList = new ArrayList<Neo4jEmployeeInterface>();
                for (Record r: records) {
                    employeesList.add(mapEmployee(r));
                }

                Record r = mr.results.get(1).records.get(0);
                Integer totalRecords = r.get(0).asInt();
                message.reply( new SearchResult(totalRecords, employeesList));
            } else {
                message.fail(0, cypherResponse.cause().getMessage());
            }
        });



    }


    private void newUser(Message message) {

        String ldapName = (String)message.body();
        Neo4jEmployeeInterface neo4jEmployee = new Neo4jEmployeeJson(ldapName);
        JsonObject newUser = neo4jEmployee.getFlatData();
        newUser.put("image", "");

        Request req = new Request();
        req.query = addUserQuery;
        req.params = Values.parameters("props", newUser.getMap()); // так

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {
            handleSingleEmployee(cypherResponse, message);
        });
    }


    /**
     * Обновить данные сотрудника.
     * @param message содержит
     *                data: новые данные пользователя в плоской схеме (как они хранятся в neo4j)
     *                id: проверенный id (проверено что id соответствует залогиненому пользователю)
     *
     */
    private void editById(Message message) {

        Neo4jEmployeeJson neo4jEmployee = (Neo4jEmployeeJson)message.body();
        JsonObject birthday = neo4jEmployee.getBirthday();
        JsonObject employeeData = neo4jEmployee.getFlatData();
        Long employeeId = neo4jEmployee.getId();


        MultiRequest fullRequest = new MultiRequest();
        Request createyReq = new Request();
        Value v;
        if (birthday == null) {
            createyReq.query = editEmployeeById;
            v = Values.parameters("props", employeeData.getMap(), "employeeId", employeeId);
        } else {
            createyReq.query = editEmployeeByIdWithDate;
            v = Values.parameters(
                    "props", employeeData.getMap(),
                    "employeeId", employeeId,
                    "day", birthday.getValue("day"),
                    "month", birthday.getValue("month")
            );
        }

        Request removeBirthday = new Request();
        removeBirthday.query = deleteEmployeeDate;
        removeBirthday.params = Values.parameters("employeeId", employeeId);
        fullRequest.requests.add(removeBirthday);

        createyReq.params = v;
        fullRequest.requests.add(createyReq);


        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, fullRequest, cypherResponse -> {
            if (cypherResponse.succeeded()) {
                MultiResult mr = (MultiResult)cypherResponse.result().body();
                List<Record> records = mr.results.get(1).records;
                responseSingleEmployee(records, message);
            } else {
                message.fail(0, cypherResponse.cause().getMessage());
            }
        });
    }

    private void setImage(Message message) {

        JsonObject data = (JsonObject)message.body();
        Request req = new Request();
        req.query = setEmployeeImage;
        req.params = Values.parameters(
                "employeeId", data.getLong("employeeId"),
                "image", data.getString("image")
        );

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {
            handleSingleEmployee(cypherResponse, message);
        });
    }


    private void handleSingleEmployee(AsyncResult<Message<Object>> resp, Message message) {
        if (resp.succeeded()) {
            SingleResult singleResult = (SingleResult) resp.result().body();
            List<Record> records = singleResult.records;
            responseSingleEmployee(records, message);
        } else {
            message.fail(0, resp.cause().getMessage());
        }
    }

    private void responseSingleEmployee(List<Record> records, Message message) {
        int size = records.size();
        if (size == 0) {
            message.reply(null);
            return;
        } else if (size > 1) {
            String errorMessage = "Найдено больше одного пользователя";
            message.fail(0, errorMessage);
            logger.error(errorMessage);
            return;
        }

        Record r0 = records.get(0);
            Neo4jEmployeeInterface employee =  mapEmployee(r0);
            message.reply(employee);

    }

    private Neo4jEmployeeInterface mapEmployee (Record r)  {
        Value e = r.get(0);
        Node day = r.get(1) instanceof NullValue ? null : r.get(1).asNode();
        Node month = r.get(2) instanceof NullValue ? null : r.get(2).asNode();
        List<Object> departments = r.get(3) instanceof NullValue ? null : r.get(3).asList();

        Node n = e.asNode();

        return new Neo4jEmployeeJson().initValue(n, day, month, departments);
    }



}
