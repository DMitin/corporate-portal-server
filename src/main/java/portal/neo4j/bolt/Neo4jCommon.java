package portal.neo4j.bolt;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import org.apache.commons.lang3.NotImplementedException;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.Values;
import org.neo4j.driver.v1.types.Node;
import portal.neo4j.CypherFactory;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.pojo.neo4j.MultiRequest;
import portal.pojo.neo4j.MultiResult;
import portal.pojo.neo4j.Neo4jRequest;
import portal.pojo.neo4j.Request;
import portal.pojo.neo4j.SingleResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Mitin on 08.06.2016.
 */
public class Neo4jCommon extends AbstractVerticle {

    String createCommon = " MERGE (t:Tag { name:'Clojure' }) RETURN t";
    String getLabel = "START n=Node({id}) RETURN labels(n)";


    public static String GET_LABELS_EVENT_NAME = "neo4j.common.getLabels";

    @Override
    public void start() {
        EventBus eb = vertx.eventBus();
        eb.consumer("neo4j.common.relation.add", this::addRelationship);
        eb.consumer("neo4j.common.object.add", this::addEntity);
        eb.consumer(GET_LABELS_EVENT_NAME, this::getLabels);
    }

    private void addRelationship(Message message) {

        Relationship relationship = (Relationship) message.body();
        String query = CypherFactory.createQueryAddRelationship(relationship.name);
        Request req = new Request(query,
                Values.parameters(
                        "from", relationship.from,
                        "to", relationship.to));

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {

            if (cypherResponse.succeeded()) {
                mapSingleResult(cypherResponse, message, v-> {
                    org.neo4j.driver.v1.types.Relationship rel = v.asRelationship();
                    Relationship relationhipResponse = Relationship.initValue(rel);
                    message.reply(relationhipResponse);
                });

            } else {
                message.fail(0, cypherResponse.cause().getMessage());
            }
        });
    }

    private void addEntity(Message message) {

        Entity entity = (Entity) message.body();
        String query = CypherFactory.createQueryAddCommonMerge(entity.label);
        Request req = new Request(query, Values.parameters("name", entity.name));

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {

            if (cypherResponse.succeeded()) {
                mapSingleResult(cypherResponse, message, v-> {
                    Node n = v.asNode();
                    Entity entityResponse = Entity.initValue(n);
                    message.reply(entityResponse);
                });
            } else {
                message.fail(0, cypherResponse.cause().getMessage());
            }
        });
    }

    private void getLabels(Message message) {

        List<Long> ids = (List) message.body();
        Neo4jRequest neo4jRequest = null;
        if (ids.size() == 0) {
            message.fail(0, "Передан пустой список");
        } else if (ids.size() == 1) {
            neo4jRequest = new Request(getLabel, Values.parameters("id", ids.get(0)));
        } else {
            MultiRequest fullRequest = new MultiRequest();
            for (Long id : ids ) {
                Request req = new Request(getLabel, Values.parameters("id", id));
                fullRequest.requests.add(req);
            }
            neo4jRequest = fullRequest;
        }

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, neo4jRequest, cypherResponse -> {

            if (cypherResponse.succeeded()) {
                Object body =  cypherResponse.result().body();
                if (body instanceof SingleResult) {
                    SingleResult singleResult = (SingleResult)body;
                    Value value = singleResult.checkAndGetSingleRecord(message).get(0);
                    List labels = value.asList();
                    if (labels.size() != 1) {
                        message.fail(0, "Неправильное количество labels");
                    } else {
                        //кодер только для ArrayList
                        message.reply(new ArrayList<>(labels));
                    }
                } else {
                    MultiResult multiResult = (MultiResult)body;
                    List<String> labels = new ArrayList<String>();
                    for(SingleResult singleResult: multiResult.results) {
                        Value value = singleResult.checkAndGetSingleRecord(message).get(0);
                        List labelsOfResp = value.asList();
                        if (labelsOfResp.size() != 1) {
                            message.fail(0, "Неправильное количество labels");
                        } else {
                           labels.add((String)labelsOfResp.get(0));
                        }
                    }
                    message.reply(labels);
                }


            } else {
                message.fail(0, cypherResponse.cause().getMessage());
            }
        });

    }

    private void mapSingleResult(AsyncResult<Message<Object>> cypherResponse, Message message, Handler<Value> handler) {
        SingleResult singleResult = (SingleResult) cypherResponse.result().body();
        List<Record> records = singleResult.records;
        if (records.size() != 1) {
            message.fail(0, "Неправильное количество строк в ответе neo4j");
        } else {
            Record r = records.get(0);
            handler.handle(r.get(0));
        }
    }


}
