package portal.neo4j.bolt;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.types.Node;
import portal.pojo.neo4j.MultiRequest;
import portal.pojo.neo4j.MultiResult;
import portal.pojo.neo4j.Request;
import portal.pojo.neo4j.SingleResult;
import portal.sharedData.NodeLabels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Denis Mitin on 31.05.2016.
 */
public class Neo4jBolt extends AbstractVerticle {

    public static final String NEO4J_RUN_CYPHER_BOLT = "neo4j.runCypher.bolt";
    static Logger logger = LoggerFactory.getLogger(Neo4jBolt.class.getName());

    @Override
    public void start() {

        Driver driver = GraphDatabase.driver( "bolt://localhost", AuthTokens.basic( "neo4j", "secr8t" ) );


        EventBus eb = vertx.eventBus();
        eb.consumer(NEO4J_RUN_CYPHER_BOLT, runCypherMessage -> {

            List<Request> requests = null;
            List<Record>[] response = null;
            Object body = runCypherMessage.body();
            if (body instanceof Request) {
                requests = new ArrayList<Request>();
                requests.add((Request)body);
                response = new List[1];
            } else if (body instanceof MultiRequest) {
                MultiRequest mr = (MultiRequest) body;
                requests = mr.requests;
                response = new List[mr.requests.size()];
            } else {
                logger.fatal("Запорс неизвестного типа");
            }


            List<Request> finalRequests = requests;
            List<Record>[] finalResponse = response;
            vertx.executeBlocking(future-> {


                List<Record> records = null;
                Transaction tx = null;
                try (   Session session = driver.session();
                        /*Transaction tx = session.beginTransaction()*/ ) {

                    tx = session.beginTransaction();
                    for (int i = 0; i < finalRequests.size(); i ++) {
                        Request req = finalRequests.get(i);
                        logger.debug(req.query);
                        logger.debug(Json.encodePrettily(req.params.toString()));
                        StatementResult result = tx.run(req.query, req.params);
                        records  = result.list();
                        cashEntityIds(records);

                        finalResponse[i] = records;
                        logger.debug(records);
                    }
                    /*
                    logger.debug(req.query);
                    logger.debug(Json.encodePrettily(req.params.toString()));
                    tx = session.beginTransaction();
                    StatementResult result = tx.run(req.query, req.params);
                    */
                    tx.success();
                } catch (Exception e) {
                    e.printStackTrace();
                    future.fail(e.getMessage());
                    tx.failure();
                    return;
                } finally {
                    if (tx.isOpen()) {
                        logger.error("close neo4j transaction");
                        tx.close();
                    }
                }
                future.complete(finalResponse);

            }, event -> {
                if (event.succeeded()) {
                    List<Record>[] records = (List<Record>[])event.result();
                    if (records.length == 1) {
                        SingleResult singleResult = new SingleResult(records[0]);
                        runCypherMessage.reply(singleResult);
                    } else {
                        MultiResult multiResult = new MultiResult(records);
                        runCypherMessage.reply(multiResult);
                    }
                } else {
                    String message = (String)event.cause().getMessage();
                    runCypherMessage.fail(0, message);
                }
            });



        });
    }

    private void cashEntityIds(List<Record> records) {
        Map<Long, String> ids = new HashMap<Long, String>();
        records.forEach(record -> {

            for (int i = 0; i < record.size(); i ++) {
                Value v = record.get(i);
                String name = v.type().name();

                if (name.equals("NODE")) {
                    Node node = v.asNode();
                    NodeLabels.addEntityToCash(node, vertx);
                } else if (name.equals("LIST OF ANY?")) {
                    List nodes = v.asList();
                    nodes.forEach(n-> {
                        if (n instanceof Node) {
                            NodeLabels.addEntityToCash((Node)n, vertx);
                        }
                    });
                }
            }
        });
        logger.debug(ids);
    }

}
