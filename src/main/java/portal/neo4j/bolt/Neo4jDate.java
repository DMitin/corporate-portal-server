package portal.neo4j.bolt;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Values;
import org.neo4j.driver.v1.types.Node;
import portal.pojo.DayBirthdays;
import portal.pojo.employee.EmployeeSmall;
import portal.pojo.neo4j.MonthBirthdays;
import portal.pojo.neo4j.Request;
import portal.pojo.neo4j.SingleResult;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.toIntExact;

/**
 * Created by Denis Mitin on 03.06.2016.
 */
public class Neo4jDate extends AbstractVerticle {

    Logger logger = LoggerFactory.getLogger(Neo4jDate.class.getName());
    String getAllOfMonth = "OPTIONAL MATCH (m:MONTH {number: {month}})--(d)--(e) "
             + "RETURN d, collect(e) "
             + "ORDER BY d.number ";

    @Override
    public void start() {
        EventBus eb = vertx.eventBus();
        eb.consumer("neo4j.date.month", this::getEventhOfMonth);
    }

    private void getEventhOfMonth(Message message) {
        Integer month = (Integer)message.body();
        logger.debug(month);

        /*
        JsonObject params = new JsonObject()
                .put("month", month);

        //JsonObject req = Neo4jRequestMapping.mapLegacyRequest(addUserQuery, params);
        JsonObject req = Neo4jRequestMapping.mapTransactionalRequests(
                Neo4jRequestMapping.mapTransactionalRequest(getAllOfMonth, params)
        );
        */
        Request req = new Request(getAllOfMonth,
                Values.parameters("month", month)
        );

        vertx.eventBus().send(Neo4jBolt.NEO4J_RUN_CYPHER_BOLT, req, cypherResponse -> {


            SingleResult sr = (SingleResult)cypherResponse.result().body();
            ArrayList<DayBirthdays> dayArray = new ArrayList<DayBirthdays>();

            for (int i = 0; i < sr.records.size(); i ++) {

                Record r = sr.records.get(i);
                if ( !r.get(0).isNull() ) {
                    Node dayNode = r.get(0).asNode();
                    int dayNumber =  toIntExact((Long)dayNode.asMap().get("number"));

                    ArrayList<EmployeeSmall> employees = new ArrayList<EmployeeSmall>();
                    List<Node> list = (List<Node>)r.get(1).asList(Values.ofNode());
                    for (Node n : list) {
                        try {
                            EmployeeSmall es = EmployeeSmall.initValue(n);
                            employees.add(es);
                        } catch (InvocationTargetException | IllegalAccessException exception) {
                            message.fail(0, exception.getMessage());
                            return;
                        }
                    }

                    DayBirthdays dayBirthdays = new DayBirthdays();
                    dayBirthdays.day = dayNumber;
                    dayBirthdays.employees = employees;
                    dayArray.add(dayBirthdays);
                }
            }

            MonthBirthdays m = new MonthBirthdays();
            m.birthdayses = dayArray;
            message.reply(m);

        });

    }
}
