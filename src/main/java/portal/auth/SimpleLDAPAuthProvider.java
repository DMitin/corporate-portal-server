package portal.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by PGaev on 20.10.2015.
 */
public class SimpleLDAPAuthProvider implements AuthProvider {
    private static final String TEST_USER = "auth_as_test";

    private static final String LDAP_URL = "ldap://domain.corp:389";
    private static final String AUTH_MECHANISM = "simple";
    private static final String DOMAIN_PREFIX = "domain\\";

    private Vertx vertx;
    public SimpleLDAPAuthProvider(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
        long startTime = System.currentTimeMillis();

        String username = authInfo.getString("ldapName");
        if (username == null || username.isEmpty()) {
            resultHandler.handle(Future.failedFuture("authInfo must contain username in \'username\' field"));
            return;
        }
        String password = authInfo.getString("password");
        if (password == null || username.isEmpty()) {
            resultHandler.handle(Future.failedFuture("authInfo must contain password in \'password\' field"));
            return;
        }


        vertx.executeBlocking(future -> {
            try {
                if (!isValid(username, password)) {
                    future.fail("Invalid username/password");
                    return;
                }
            } catch (NamingException e) {
                future.fail("Failure in authentication");
                return;
            }
            vertx.eventBus().send("neo4j.api.getEmployeeByAuthName", username, resp-> {
                if (resp.succeeded()) {
                    System.out.println("Получили данные пользователя из neo4j");
                    JsonObject userData = (JsonObject) resp.result().body();
                    System.out.println("auth user: " + userData);
                    SharedData sd = vertx.sharedData();
                    LocalMap<String, JsonObject> map = sd.getLocalMap("authUserData");
                    map.put(username, userData);
                }
                future.complete(new PortalUser(authInfo));
            });
        }, resultHandler);
    }



    protected boolean isValid(String username, String password) throws NamingException {
        if (TEST_USER.equals(username)) {
            return true;
        }

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAP_URL);


        env.put(Context.SECURITY_AUTHENTICATION, AUTH_MECHANISM);
        env.put(Context.SECURITY_PRINCIPAL, username.toLowerCase().startsWith(DOMAIN_PREFIX) ? username : DOMAIN_PREFIX.concat(username));
        env.put(Context.SECURITY_CREDENTIALS, password);

        // Create the initial context
        try {
            DirContext ctx = new InitialDirContext(env);
        } catch (AuthenticationException e) {
            return false;
        }
        return true;
    }

    private List<String> getRoles(String username) {
        return Arrays.asList("employee");
    }
}
