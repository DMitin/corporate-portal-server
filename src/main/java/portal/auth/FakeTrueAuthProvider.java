package portal.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

public class FakeTrueAuthProvider implements AuthProvider {


    private static final Logger LOGGER = LoggerFactory.getLogger(FakeTrueAuthProvider.class);
    private Vertx vertx;
    public FakeTrueAuthProvider(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {

        String ldapName = authInfo.getString("ldapName");
        if (ldapName == null || ldapName.isEmpty()) {
            resultHandler.handle(Future.failedFuture("authInfo must contain username in \'username\' field"));
            return;
        }
        String password = authInfo.getString("password");
        if (password == null || password.isEmpty()) {
            resultHandler.handle(Future.failedFuture("authInfo must contain password in \'password\' field"));
            return;
        }

        LOGGER.debug("Авторизую пользователя с данными: " + authInfo);
        resultHandler.handle(Future.succeededFuture(new PortalUser(authInfo)));

    }

}
