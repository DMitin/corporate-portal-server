package portal.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

/**
 * Created by Denis Mitin on 29.07.2016.
 */
public class FakeFalseAuthProvider implements AuthProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(FakeFalseAuthProvider.class);
    private Vertx vertx;
    public FakeFalseAuthProvider(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
        resultHandler.handle(Future.failedFuture("FakeFalseAuthProvider"));
    }
}
