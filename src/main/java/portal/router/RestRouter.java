package portal.router;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import portal.enums.AuthenticationType;

/**
 * Created by DMitin on 25.03.2016.
 */
public class RestRouter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestRouter.class);

    public static void initRouter(Router router, Vertx vertx, JsonObject config) {

        router.route().handler(CookieHandler.create());
        router.route().handler(BodyHandler.create().setBodyLimit(1024 * 1024));
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
        router.route().handler(UserSessionHandler.create(null));

        /*
            Монтируем authRouter перед проверкой авторизации, чтоб методы авторизации
            были доступны неавторизованным пользователям
         */
        String authType = config.getString("auth.type", AuthenticationType.FAKE_TRUE);
        Router authRouter = Router.router(vertx);
        AuthRouter.initRouter(authRouter, vertx, authType);
        router.mountSubRouter("/auth", authRouter);

        /*
            Проверка что пользователь авторизован.
            Если авторизован, выполняем слейдующие хендлеры, если не авторизован, то посылаем
            401 статус (Unauthorized )
         */
        router.route().handler(rc -> {
            Session session = rc.session();
            User user = rc.user();
            if (user != null) {
                LOGGER.debug("user authed with principals: " + user.principal());
                rc.next();
            } else {
                LOGGER.debug("user not auth");
                rc.response()
                        .setStatusCode(401)
                        .end();

            }


        });

        Router userRouter = Router.router(vertx);
        UserRouter.initRouter(userRouter, vertx);
        router.mountSubRouter("/user", userRouter);

        Router dateRouter = Router.router(vertx);
        DateRouter.initRouter(dateRouter, vertx);
        router.mountSubRouter("/date", dateRouter);

        Router locationRouter = Router.router(vertx);
        LocationRouter.initRouter(locationRouter, vertx);
        router.mountSubRouter("/location", locationRouter);

        Router commonRouter = Router.router(vertx);
        CommonRouter.initRouter(commonRouter, vertx);
        router.mountSubRouter("/common", commonRouter);
    }
}
