package portal.router;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import portal.Handlers;
import portal.pojo.Entities;

/**
 * Created by Denis Mitin on 25.05.2016.
 */
public class LocationRouter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationRouter.class);
    static private Vertx lVertx;

    public static void initRouter(Router router, Vertx vertx) {
        lVertx = vertx;
        router.route()
                .path("/")
                .method(HttpMethod.POST).handler(LocationRouter::addLocation);

        router.route()
                .path("/search")
                .method(HttpMethod.POST).handler(LocationRouter::searchLocation);

        router.route()
                .path("/search")
                .method(HttpMethod.POST).handler(LocationRouter::searchLocation);
    }

    private static void addLocation(RoutingContext rc) {
        JsonObject params = new JsonObject()
                .put("label", "LOCATION")
                .put("name", "212B");
        lVertx.eventBus().send("neo4j.common.add", params, resp-> {
            if (resp.succeeded()) {
                JsonObject id = (JsonObject) resp.result().body();
                rc.response().end(id.toString());
            } else {
                Handlers.handleError(resp, rc, "Ошибка при создании новой локации (кабинета) " + params.encodePrettily());
            }
        });

        //rc.response().end("add location");
    }

    private static void searchLocation(RoutingContext rc) {
        JsonObject params = new JsonObject()
                .put("label", "LOCATION")
                .put("name", "212B");
        lVertx.eventBus().send("neo4j.common.search", params, resp-> {
            if (resp.succeeded()) {
                Entities entities = (Entities) resp.result().body();
                rc.response().end(Json.encode(entities.entities));
            } else {
                Handlers.handleError(resp, rc, "Ошибка при поиске локации (кабинета) " + params.encodePrettily());
            }
        });


    }

}
