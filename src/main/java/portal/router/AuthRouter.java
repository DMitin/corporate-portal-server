package portal.router;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import portal.sharedData.AuthUserData;
import portal.Utils;
import portal.auth.FakeFalseAuthProvider;
import portal.auth.FakeTrueAuthProvider;
import portal.auth.SimpleLDAPAuthProvider;
import portal.enums.AuthenticationType;
import portal.pojo.UserData;
import portal.pojo.neo4j.Neo4jEmployeeJson;

/**
 * Created by DMitin on 28.03.2016.
 */
public class AuthRouter {

    static Logger logger = LoggerFactory.getLogger("AuthRouter");
    private static AuthProvider authProvider;
    public static final String FAKE_AUTH = "fake";

    public static void initRouter(Router router, Vertx vertx, String authType) {

        logger.debug("authType: " + authType);
        //AuthProvider authProvider = null;

        if (authType.equals(AuthenticationType.FAKE_TRUE)) {
            authProvider = new FakeTrueAuthProvider(vertx);
        } else if (authType.equals(AuthenticationType.FAKE_FALSE)) {
            authProvider =  new FakeFalseAuthProvider(vertx);
        } else if (authType.equals(AuthenticationType.LDAP)) {
            authProvider =  new SimpleLDAPAuthProvider(vertx);
        } else {
            throw new IllegalArgumentException("Неправильное значение authType");
        }


        /*
            Авторизация пользователя в системе
         */
        router.route()
                .path("/login")
                .method(HttpMethod.POST).handler(rc -> {
            logger.debug(Utils.logRequest(rc));

            User user = rc.user();
            //UserData userData = Utils.getUserId(vertx, rc);
            if (user == null) { //ещё не авторизован в системе
                JsonObject authData = rc.getBodyAsJson();
                authenticateUser(vertx, rc, authData); // выполняю авторизацию
            } else { // авторизован, надо только вернуть данные
                String ldapName = user.principal().getString("ldapName");
                UserData userData = AuthUserData.getUserData(ldapName, vertx);
                UserRouter.getUserById(vertx, rc, userData.getId()); // отправляю данные пользователя
            }
        });

        /*
            Выход пользователя из системы.
         */
        router.route()
                .path("/logout")
                .method(HttpMethod.POST).handler(rc -> {
            logger.debug(Utils.logRequest(rc));

            //Long userId = Utils.getUserId(vertx, rc);
            User user = rc.user();
            if (user == null) { //ещё не авторизован в системе
                //Если пытается выйти неавторизованный пользователь то посылается 403
                rc.response()
                        .setStatusCode(401)
                        .end("Пользователь не авторизован");
            } else { // авторизован, надо только вернуть данные
                rc.clearUser();
                String ldapName = user.principal().getString("ldapName");
                AuthUserData.removeUserData(ldapName, vertx);
                rc.response()
                        .end("Пользователь удачно вышел с портала");
            }
        });


    }


    /**
     * Аутентификация пользователя [/login POST]
     *  1. аутентификация у authProvider
     *  2. поиск в neo4j по ldapName
     *  3. отсылает ответ серверу
     *
     * @param vertx
     * @param rc
     * @param authData - credentials входа (ldapName, password)
     */
    private static void authenticateUser(Vertx vertx, RoutingContext rc, JsonObject authData) {
        //@TODO продумать схему, чтоб этот метод сам не отсылал ответы черех rc, а просто возвращал статус операции
        authProvider.authenticate(authData, authResponse -> {
            if (authResponse.succeeded()) {

                String ldapName = authResponse.result().principal().getString("ldapName");
                // Поиск пользователя в neo4j по ldapName
                vertx.eventBus().send("neo4j.user.searchByLdap", ldapName, resp -> { //JsonArray
                    if (resp.succeeded()) {

                        Neo4jEmployeeJson employee = (Neo4jEmployeeJson) resp.result().body();
                        User user = authResponse.result();
                        if (employee != null) {
                            setUserAuth(vertx, rc, user, employee, ldapName);
                        } else {
                            addNewUser(vertx, rc, user, ldapName);
                        }
                    } else {
                        rc.response().end(resp.cause().getMessage());
                    }
                });

            } else {
                rc.response()
                        .setStatusCode(401)
                        .end("Пользователь не авторизован");
            }
        });
    }


    /**
     * Добавление нового пользователя
     */
    private static void addNewUser(Vertx vertx, RoutingContext rc, User user, String ldapName) {
        vertx.eventBus().send("neo4j.user.newUser", ldapName, resp -> {
            if (resp.succeeded()) {
                Neo4jEmployeeJson employee = (Neo4jEmployeeJson) resp.result().body();

                // так как у нас операция создания, то по любому результат состоит из одного JsonObject
                // но всё равно для безопасности оставляю проверку
                /*
                if (userJsonArray.size() == 1) {
                    JsonObject userData = userJsonArray.getJsonObject(0);
                    setUserAuth(vertx, rc, user, userData, ldapName);
                } else {
                    // при создании вернулось несколько объектов пользователя, что то пошло не так
                    Utils.unknownError(rc, "При создании пользователя, вернулось неправильное колисество объектов");
                }
                */
                setUserAuth(vertx, rc, user, employee, ldapName);
            } else {
                rc.response().end(resp.cause().getMessage());
            }
        });
    }

    /**
     * При удачной авторизации, записываем информацию о пользователе в контекст
     * и заканчивает http запрос
     *
     * @param vertx
     * @param rc
     * @param user - пользователь в терминах vert.x
     * @param employee - json с информацией пользователя (neo4j)
     * @param ldapName - ldapName
     */
    private static void setUserAuth(Vertx vertx, RoutingContext rc, User user, Neo4jEmployeeJson employee, String ldapName) {

        rc.setUser(user);
        UserData userData = new UserData(employee.getId(), employee.isHR());
        AuthUserData.saveUserData(ldapName, vertx, userData);

        rc.setUser(user);
        Utils.setCommonHeaders(rc.response())
            .end(Json.encode(employee.toUiEmployee()));
    }



}
