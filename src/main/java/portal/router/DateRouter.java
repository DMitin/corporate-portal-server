package portal.router;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import portal.Handlers;
import portal.Utils;
import portal.pojo.neo4j.MonthBirthdays;

/**
 * Created by Denis Mitin on 23.05.2016.
 */
public class DateRouter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateRouter.class);
    static private Vertx lVertx;

    public static void initRouter(Router router, Vertx vertx) {
        lVertx = vertx;
        router.route()
                .path("/month/:number")
                .method(HttpMethod.GET).handler(DateRouter::getBirthdaysInMonth);
    }


    private static void getBirthdaysInMonth(RoutingContext rc) {
        LOGGER.debug(Utils.logRequest(rc));
        Integer number = Integer.valueOf(rc.request().getParam("number"));

        lVertx.eventBus().send("neo4j.date.month", number, resp-> {
            if (resp.succeeded()) {
                MonthBirthdays mb = (MonthBirthdays) resp.result().body();
                Utils.setCommonHeaders(rc.response())
                        .end(Json.encodePrettily(mb));
                //rc.response().end(Json.encodePrettily(mb));
            } else {
                Handlers.handleError(resp, rc, "Ошибка при получении Дней рождениий для месяца " + number);
            }
        });
    }
}
