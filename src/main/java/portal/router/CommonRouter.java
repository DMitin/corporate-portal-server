package portal.router;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import portal.enums.EntityEnum;
import portal.neo4j.bolt.Neo4jCommon;
import portal.sharedData.AuthUserData;
import portal.Handlers;
import portal.Server;
import portal.Utils;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.pojo.UserData;
import portal.sharedData.NodeLabels;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Created by Denis Mitin on 27.05.2016.
 */
public class CommonRouter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonRouter.class);
    static private Vertx lVertx;

    public static void initRouter(Router router, Vertx vertx) {
        lVertx = vertx;
        router.route()
                .path("/relationship")
                .method(HttpMethod.POST).handler(CommonRouter::createRelationship);

        router.route()
                .path("/object")
                .method(HttpMethod.POST).handler(CommonRouter::createObject);
    }

    private static void createObject(RoutingContext rc) {

        User user = rc.user();
        String ldapName = user.principal().getString("ldapName");
        UserData userData = AuthUserData.getUserData(ldapName, lVertx);

        JsonObject data = rc.getBodyAsJson();
        Entity entity = Json.decodeValue(data.toString(), Entity.class);
        Set<ConstraintViolation<Object>> violations = Server.validator.validate(entity);
        if (!violations.isEmpty()) {
            Utils.sendValidationErrors(violations, rc);
            return;
        }

        //resolveNodeLabel(entity.id, label ->{
        if (entity.label.equals(EntityEnum.Department)) {
            if (userData.isHr()) {
                createEntity(rc, entity);
            } else {
                rc.response()
                        .setStatusCode(403)
                        .end("Только HR может добавлять Department");
            }
        } else {
            createEntity(rc, entity);
        }
    }

    private static void createEntity(RoutingContext rc, Entity entity) {
        lVertx.eventBus().send("neo4j.common.object.add", entity, resp-> {
            if (resp.succeeded()) {
                Entity saved = (Entity) resp.result().body();
                String response = Json.encode(saved);
                Utils.setCommonHeaders(rc.response())
                        .end(response);
            } else {
                Handlers.handleError(resp, rc, "Ошибка при создании common object " + entity);
            }
        });
    }

    private static void createRelationship(RoutingContext rc) {
        JsonObject data = rc.getBodyAsJson();
        Relationship rel = Json.decodeValue(data.toString(), Relationship.class);
        Set<ConstraintViolation<Object>> violations = Server.validator.validate(rel);

        User user = rc.user();
        String ldapName = user.principal().getString("ldapName");
        UserData userData = AuthUserData.getUserData(ldapName, lVertx);

        if (!violations.isEmpty()) {
            Utils.sendValidationErrors(violations, rc);
            return;
        }

        resolveNodeLabel(rel.from, rel.to, (label1, label2)-> {
            if (validateRelationshipLabels(rel.name, label1, label2)) {

                if (rel.name.equals(EntityEnum.Department)) {
                    if (userData.isHr()) {
                        Handlers.callEventAndResponse(rc,rel, "neo4j.common.relation.add", body-> {
                            Relationship saved = (Relationship) body;

                            AuthUserData.saveUserData(ldapName, lVertx, new UserData(userData.getId(), true));
                            String response = Json.encode(saved);
                            Utils.setCommonHeaders(rc.response())
                                    .end(response);
                        });
                    } else {
                        rc.response()
                                .setStatusCode(401)
                                .end("Не хватает прав. Только HR может добавлять в department");
                    }
                } else { // не Department, не нужно прав HR
                    createRelationship(rel, rc);
                }
            } else {
                rc.response()
                        .setStatusCode(405)
                        .end("Неподдерживаемый тип relationhip");
            }
        });

        /*
        SharedData sd = lVertx.sharedData();
        LocalMap<Long, String> ids = sd.getLocalMap("Labels");
        String labelFrom = ids.get(rel.from);
        LOGGER.debug("labelFrom = " + labelFrom);
        String labelTo = ids.get(rel.to);
        LOGGER.debug("labelTo = " + labelTo);
        */



    }

    private static void createRelationship(Relationship rel, RoutingContext rc) {
        lVertx.eventBus().send("neo4j.common.relation.add", rel, resp-> {
            if (resp.succeeded()) {
                Relationship saved = (Relationship) resp.result().body();
                String response = Json.encode(saved);
                Utils.setCommonHeaders(rc.response())
                        .end(response);
            } else {
                Handlers.handleError(resp, rc, "Ошибка при создании нового Relationship " + rel.toString());
            }
        });
    }

    private static void resolveNodeLabel(Long id1, Long id2, BiConsumer<String, String> handler) {
        ArrayList<Long> notFound = new ArrayList();

        String label1 = NodeLabels.getLabelOfNode(id1, lVertx);
        if (label1 == null)  {
            notFound.add(id1);
        }

        String label2 = NodeLabels.getLabelOfNode(id2, lVertx);
        if (label2 == null)  {
            notFound.add(id2);
        }

        if (notFound.isEmpty()) {
            handler.accept(label1, label2);
        } else {
            lVertx.eventBus().send(Neo4jCommon.GET_LABELS_EVENT_NAME, notFound, resp-> {
                if (resp.succeeded()) {
                    List<String> foundLabels = (List<String>)resp.result().body();
                    String resLabel1 = label1 == null ? foundLabels.get(0) : label1;
                    String resLabel2 = null;
                    if (label2 == null) {
                        if (label1 == null) {
                            resLabel2 = foundLabels.get(1);
                        } else {
                            resLabel2 = foundLabels.get(0);
                        }
                    } else {
                        resLabel2 = label2;
                    }
                    handler.accept(resLabel1, resLabel2);
                } else {
                    handler.accept(null,null);
                }
            });
        }

    }

    private static boolean validateRelationshipLabels(String name, String label1, String label2) {
        if (name.equals(EntityEnum.Department)) {
            return label1.equals(EntityEnum.EMPLOYEE) && label2.equals(EntityEnum.Department);
        } else {
            // пока что всё разпешаем
            return true;
        }
    }
}
