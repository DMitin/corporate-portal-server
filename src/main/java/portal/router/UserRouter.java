package portal.router;

import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.SystemUtils;
import portal.sharedData.AuthUserData;
import portal.Handlers;
import portal.Server;
import portal.Utils;


import portal.pojo.UserData;
import portal.pojo.employee.Employee;
import portal.pojo.employee.EmployeeSearch;
import portal.enums.EmployeeStatuses;
import portal.pojo.neo4j.Neo4jEmployeeInterface;
import portal.pojo.neo4j.Neo4jEmployeeJson;
import portal.pojo.neo4j.SearchResult;

import javax.validation.ConstraintViolation;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by DMitin on 31.03.2016.
 */
public class UserRouter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRouter.class);
    static Map<String, String> responses = new HashMap<>();
    static String userInfoResponse = "responses/userInfo.json";

    public static int DEFAULT_PAGE_SIZE = 5;


    static private Vertx lVertx;
    public static void initRouter(Router router, Vertx vertx) {

        FileSystem fs = vertx.fileSystem();
        lVertx = vertx;

        // получить информацию о залогиненном пользователе
        router.route()
                .path("/current")
                .method(HttpMethod.GET).handler(UserRouter::current);

        // получить информацию о пользователе по id
        router.route()
                .path("/:id")
                .method(HttpMethod.GET).handler(UserRouter::getUserById);

        // поменять данные пользователя
        router.route()
                .path("/")
                .method(HttpMethod.PUT).handler(UserRouter::editUser);

        // поиск пользователя по текстовой строке (имя/фамилия/отчество)
        router.route()
                .path("/search")
                .method(HttpMethod.POST).handler(UserRouter::searchUser);

        router.route()
                .path("/loadImage")
                .method(HttpMethod.POST).handler(UserRouter::loadImage);
    }


    /**
     * Обработчик http запроса /:id (GET)
     * @param rc
     */
    private static void getUserById(RoutingContext rc) {
        LOGGER.debug(Utils.logRequest(rc));
        Long id = Long.valueOf(rc.request().getParam("id"));

        getUserById(lVertx, rc, id);
    }


    /**
     * Обработчик http запроса /current
     * @param rc
     */
    private static void current(RoutingContext rc) {
        //можно не проверять что id == null, этот метод только для авторизованных
        User user = rc.user();
        String ldapName = user.principal().getString("ldapName");
        UserData userData = AuthUserData.getUserData(ldapName, lVertx);
        getUserById(lVertx, rc, userData.getId());
    }

    /**
     * Обработчик http запроса /:id (POST)
     * @param rc
     */
    private static void editUser(RoutingContext rc)  {
        LOGGER.debug(Utils.logRequest(rc));
        JsonObject data = rc.getBodyAsJson();


        Employee employee = Json.decodeValue(data.toString(), Employee.class);
        Set<ConstraintViolation<Object>> violations = Server.validator.validate(employee);
        if (!violations.isEmpty()) {
            Utils.sendValidationErrors(violations, rc);
            return;
        }

        Long userId = data.getLong("id");
        User user = rc.user();
        String ldapName = user.principal().getString("ldapName");
        UserData userData = AuthUserData.getUserData(ldapName, lVertx);

        /*
            немного излишняя проверка, мы можем просто взять id пользователя из сессии
            и проигнорировать id который пришёл от пользователя, но посылая 403 ошибку
            просто сигнализируем на UI
            HR может менять данные других пользователей
        */
        if (userId.equals(userData.getId()) || userData.isHr())  {
            Neo4jEmployeeInterface requestEmployee = new Neo4jEmployeeJson().initValue(employee, ldapName);
            requestEmployee.setStatus(EmployeeStatuses.VALID);

            lVertx.eventBus().send("neo4j.user.editById", requestEmployee, resp -> {
                if (resp.succeeded()) {
                    Neo4jEmployeeJson neo4jEmployee = (Neo4jEmployeeJson)resp.result().body();
                    String response = Json.encode(neo4jEmployee.toUiEmployee());

                    LOGGER.debug(Utils.logResponse(rc, response));
                    Utils.setCommonHeaders(rc.response())
                            .end(response);
                } else {
                    Handlers.handleError(resp, rc, "neo4j.user.editById");
                }
            });


        } else {
            rc.response()
                    .setStatusCode(403)
                    .end("Запрещено менять данные другого пользователя");
        }

    }

    /**
     * Обработчик http запроса /search (POST)
     * @param rc
     */
    private static void searchUser(RoutingContext rc) {
        LOGGER.debug(Utils.logRequest(rc));

        JsonObject data;
        int bodySize = rc.getBody().toString().trim().length();
        if (bodySize == 0) {
            data = new JsonObject();
        } else {
            data = rc.getBodyAsJson();
        }


        Integer pageNumber = null;
        try {
            pageNumber = data.getInteger("pageNumber", 0);
        } catch (ClassCastException e) {
            rc.response().setStatusCode(400).end("параметр pageNumber должен быть числом (integer)");
            return;
        }

        Integer pageSize = null;
        try {
            pageSize = data.getInteger("pageSize", DEFAULT_PAGE_SIZE);
        } catch (ClassCastException e) {
            rc.response().setStatusCode(400).end("параметр pageSize должен быть числом (integer)");
            return;
        }

        boolean descending = false;
        try {
            descending = data.getBoolean("desc", false);
        } catch (ClassCastException e) {
            rc.response().setStatusCode(400).end("параметр descending должен быть boolean");
            return;
        }

        JsonObject reqData = new JsonObject()
                .put("searchString", data.getString("searchString", "").toLowerCase())
                .put("pageSize", pageSize)
                .put("pageNumber", pageNumber);

        Integer finalPageSize = pageSize;
        boolean finalDescending = descending;
        Handlers.callEventAndResponse(lVertx, rc, reqData, "neo4j.user.searchByText", resp-> {

            SearchResult searchResponse = (SearchResult) resp;
            List<Neo4jEmployeeInterface> employeeList = searchResponse.employeeList;
            /*
            neo4jResponse.getJsonArray("records")
                    .stream()
                    .map(record->(JsonObject)record)
                    .collect(Collectors.groupingBy((this::getFirstLetter));
            */

            Map<String, List<EmployeeSearch>> unsortMap = employeeList.stream()
                    .map(i->i.toUiEmployeeSearch())
                    .collect(Collectors.groupingBy(item -> {
                        String name = item.lastName;
                        if (name.isEmpty()) {
                            return "";
                        } else {
                            return Character.toString(name.charAt(0));
                        }
                    }));


            Map<String, List<Neo4jEmployeeJson>> iterateMap;
            if (finalDescending) {
                iterateMap =  new TreeMap(unsortMap).descendingMap();
            } else {
                iterateMap = new TreeMap(unsortMap);
            }

            JsonArray searchResult = new JsonArray();

            iterateMap.forEach( (letter, array )-> {
                searchResult.add(new JsonObject()
                        .put("letter", letter)
                        .put("items", new JsonArray(array)));
            });

            JsonObject serverResponse = new JsonObject()
                    .put("searchResult", searchResult)
                    .put("totalRecords", searchResponse.totalRecords)
                    .put("pageSize", finalPageSize);

            return serverResponse.toString();
        });

    }



    private static void loadImage(RoutingContext rc) {
        Set<FileUpload> uploads = rc.fileUploads();
        FileUpload[] uploadList = uploads.toArray(new FileUpload[uploads.size()]);

        User user = rc.user();
        String ldapName = user.principal().getString("ldapName");
        UserData userData = AuthUserData.getUserData(ldapName, lVertx);

        if (uploadList.length != 1) {
            rc.response().end("No file");
        } else {
            FileUpload f1 = uploadList[0];

            String uploadedFilename = f1.uploadedFileName();
            String realUrl;
            if (SystemUtils.IS_OS_WINDOWS) {
                realUrl =  uploadedFilename.replaceAll("file-uploads\\\\", "/images/");
            } else {
                realUrl =  uploadedFilename.replaceAll("file-uploads", "images");
            }


            JsonObject requestData = new JsonObject()
                    .put("employeeId", userData.getId())
                    .put("image", realUrl);

            lVertx.eventBus().send("neo4j.user.setImage", requestData, resp -> {
                if (resp.succeeded()) {
                    Neo4jEmployeeJson neo4jEmployee2 = (Neo4jEmployeeJson)resp.result().body();
                    String response = Json.encode(neo4jEmployee2.toUiEmployee());

                    LOGGER.debug(Utils.logResponse(rc, response));
                    Utils.setCommonHeaders(rc.response())
                            .end(response);
                } else {
                    Handlers.handleError(resp, rc, "neo4j.user.setImage");
                }
            });
        }
    }




        /**
         * Общий метод получения пользователя по id (может вызываться и из других классов)
         * @param vertx
         * @param rc
         * @param id
         */
    public static void getUserById(Vertx vertx, RoutingContext rc, Long id) {
        //Handlers.callEventAndResponse(lVertx, rc, id, "neo4j.user.getById", null);
        String eventName = "neo4j.user.getById";
        vertx.eventBus().send("neo4j.user.getById", id, resp -> {
            if (resp.succeeded()) {
                Neo4jEmployeeJson neo4jEmployee = (Neo4jEmployeeJson)resp.result().body();
                String response = Json.encode(neo4jEmployee.toUiEmployee());

                LOGGER.debug(Utils.logResponse(rc, response));
                Utils.setCommonHeaders(rc.response())
                        .end(response);
            } else {
                Handlers.handleError(resp, rc, eventName);
            }
        });
    }

}
