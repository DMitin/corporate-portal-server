package portal;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import portal.codec.*;

import portal.enums.Environment;
import portal.pojo.Entities;
import portal.pojo.Entity;
import portal.pojo.Relationship;
import portal.pojo.neo4j.*;
import portal.router.RestRouter;


import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;

/**
 * Created by DMitin on 23.03.2016.
 */
public class Server extends AbstractVerticle {

    Logger logger = LoggerFactory.getLogger("Server");
    public static final String FAKE_AUTH = "fake";
    public static final String ENV_DEVELOPMENT = "development";

    public static Validator validator;

    public static Vertx sVertx;

    @Override
    public void start(Future<Void> fut) {

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();

        String environment = config().getString("env", Environment.ENV_PROD);
        if (environment.equals(Environment.ENV_DEVELOPMENT)) {
            VertxOptions options = new VertxOptions();
            options.setMaxEventLoopExecuteTime(Long.MAX_VALUE);
            vertx = Vertx.vertx(options);
        }
        sVertx = vertx;


        vertx.deployVerticle("portal.neo4j.bolt.Neo4jBolt");
        vertx.deployVerticle("portal.neo4j.bolt.Neo4jUser");
        vertx.deployVerticle("portal.neo4j.bolt.Neo4jDate");
        vertx.deployVerticle("portal.neo4j.bolt.Neo4jCommon");

        //vertx.eventBus().registerDefaultCodec(Integer.class, new IntegerCodec());
        vertx.eventBus().registerDefaultCodec(MonthBirthdays.class, new MonthBirthdaysCodec());
        vertx.eventBus().registerDefaultCodec(Entities.class, new EntitiesCodec());
        vertx.eventBus().registerDefaultCodec(Entity.class, new EntityCodec());
        vertx.eventBus().registerDefaultCodec(Relationship.class, new RelationshipCodec());
        vertx.eventBus().registerDefaultCodec(SingleResult.class, new SingleResultCodec());
        vertx.eventBus().registerDefaultCodec(Neo4jEmployeeJson.class, new Neo4jEmployeeJsonCodec());
        vertx.eventBus().registerDefaultCodec(Request.class, new RequestCodec());
        vertx.eventBus().registerDefaultCodec(MultiRequest.class, new MultiRequestCodec());
        vertx.eventBus().registerDefaultCodec(MultiResult.class, new MultiResultCodec());
        vertx.eventBus().registerDefaultCodec(SearchResultRecords.class, new SearchResponseRecordCodec());
        vertx.eventBus().registerDefaultCodec(SearchResult.class, new SearchResponseCodec());
        vertx.eventBus().registerDefaultCodec(ArrayList.class, new ArrayListCodec());

        /*
        vertx.deployVerticle("portal.neo4j.Neo4jEmbedded", complete -> {
            logger.info("Neo4jEmbedded started");
            fut.complete();
        });
        */

        JsonObject  config = config();

        int port = config().getInteger("http.port", 8080);
        logger.info("стартую сервер на порту: " + port);

        Router router = Router.router(vertx);
/*
        router.route().handler(rc-> {
           logger.info(Utils.logRequest(rc));
            rc.response().end("Fake response");
        });
*/
        router.route().failureHandler( new CommonExceptionHandler());
        router.route()
                .path("/user")
                .method(HttpMethod.GET).handler(rc-> {
            rc.response().putHeader("Content-type", "application/json; charset=utf-8");
            rc.response().end("Hello");
        });
/*
        router.route()
                .path("/")
                .method(HttpMethod.POST).handler(rc-> rc.response().end("sss"));
*/

        Router restAPI = Router.router(vertx);
        RestRouter.initRouter(restAPI, vertx, config);
        router.mountSubRouter("/rest", restAPI);
        //router.route("/images/*").handler(StaticHandler.create("file-uploads"));

        //router.route("/raml/*").handler(StaticHandler.create("raml"));
        router.route("/images/*").handler(StaticHandler.create("file-uploads"));
        router.route("/card/*").handler(StaticHandler.create("public"));
        router.route("/*").handler(StaticHandler.create("public"));

        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(port);
    }




}
