package portal;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import java.util.function.Function;

/**
 * Created by DMitin on 01.04.2016.
 */
public class Handlers {

    private static final Logger LOGGER = LoggerFactory.getLogger(Handlers.class);

    /**
     * Послать событие с заданным именем, и если оно завершилось ошибкой - послать статус 500.
     * Самый верхний уровень вызова, потому что работает с RoutingContext, логгирует запрос
     *
     * @param vertx
     * @param rc
     * @param data
     * @param eventName
     */
    public static void callEventAndResponse(Vertx vertx, RoutingContext rc, Object data, String eventName,
                                            Function<Object, String> mapper) {
        vertx.eventBus().send(eventName, data, resp -> {
            if (resp.succeeded()) {
                Object body = (Object) resp.result().body();
                String response;
                if (mapper == null) {
                    response = resp.result().body().toString();
                } else {
                    response = mapper.apply(body);
                    if (response == null) {
                        rc.response()
                                .setStatusCode(500)
                                .end("Ошибка " + eventName);
                        return;
                    }

                }

                LOGGER.debug(Utils.logResponse(rc, response));
                Utils.setCommonHeaders(rc.response())
                        .end(response);
            } else {
                handleError(resp, rc, eventName);
            }
        });
    }

    public static void callEventAndContinue(Object data, String eventName, Message message, Handler<Object> handler) {
        Server.sVertx.eventBus().send(eventName, data, resp -> {
            if (resp.succeeded()) {
                Object body = (Object) resp.result().body();
                handler.handle(body);
            }  else {
            message.fail(0, "Error");
        }
        });
    }


    public static void callEventAndResponse(RoutingContext rc, Object data, String eventName,
                                            Handler<Object> handler) {
        Server.sVertx.eventBus().send(eventName, data, resp -> {
            if (resp.succeeded()) {
                handler.handle(resp.result().body());
            } else {
                handleError(resp, rc, eventName);
            }
        });
    }


    public static void handleError(AsyncResult<Message<Object>> resp, RoutingContext rc, String eventName) {
        String responseMessage = resp.cause().getMessage();
        if (responseMessage.isEmpty()) {
            rc.response()
                    .setStatusCode(500)
                    .end("Ошибка " + eventName);
        } else {
            rc.response()
                    .setStatusCode(500)
                    .end("Ошибка " + responseMessage);
        }
    }

}
