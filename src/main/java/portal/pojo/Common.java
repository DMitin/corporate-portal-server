package portal.pojo;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Denis Mitin on 09.08.2016.
 */
public class Common {

    static Logger logger = LoggerFactory.getLogger(Common.class.getName());

    protected static String getOneLabel(Iterable<String> labels) {

        ArrayList<String> labelsList = new ArrayList<>();
        labels.forEach(l-> labelsList.add(l));

        if (labelsList.size() != 1) {
            logger.error("Неправильное колтчество labels");
            throw new IllegalArgumentException("Неправильное колтчество labels");
        } else {
            return labelsList.get(0);
        }
    }
}
