package portal.pojo;

import io.vertx.core.shareddata.Shareable;

/**
 * Created by Denis Mitin on 08.09.2016.
 * This class should be immutable to be threadsafe.
 */
public class UserData implements Shareable {
    private Long id;
    private boolean isHr;

    public UserData(Long id, boolean isHr) {
        this.id = id;
        this.isHr = isHr;
    }

    public Long getId() {
        return id;
    }

    public boolean isHr() {
        return isHr;
    }
}
