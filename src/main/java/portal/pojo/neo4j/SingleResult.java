package portal.pojo.neo4j;


import io.vertx.core.eventbus.Message;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Value;

import java.util.List;

/**
 * Created by Denis Mitin on 01.06.2016.
 */
public class SingleResult {
    public List<Record> records;

    public SingleResult(List<Record> records) {
        this.records = records;
    }

    public boolean isSingleValue() {
        return records.size() == 1;
    }


    public Record checkAndGetSingleRecord(Message message) {
        Record record = null;
        if(isSingleValue()) {
            record = records.get(0);
        } else {
            message.fail(0, "Больше одного значения");
            return null;
        }
        return record;
    }

}
