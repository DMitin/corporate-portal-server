package portal.pojo.neo4j;

import io.vertx.core.json.JsonObject;
import org.neo4j.driver.v1.types.Node;
import portal.pojo.employee.Employee;
import portal.pojo.employee.EmployeeSearch;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by Denis Mitin on 27.07.2016.
 */
public interface Neo4jEmployeeInterface {
    Neo4jEmployeeInterface initValue(Node employeeNode, Node dayNode, Node monthNode, List<Object> departments)
            throws InvocationTargetException, IllegalAccessException;
    public Neo4jEmployeeInterface initValue(Employee employee, String ldapName);
    Employee toUiEmployee();
    EmployeeSearch toUiEmployeeSearch();

    /**
     * Получить данные в пригодном для сохранения в neo4j виде
     * 1. birthday в виде JsonObject {day, month}
     * 2. сгенерировано поле search string
     */
    JsonObject getFlatData();

    Long getId();

    void setStatus(String status);

    String getImage();

    boolean isHR();
}
