package portal.pojo.neo4j;

import org.neo4j.driver.v1.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Mitin on 01.06.2016.
 */
public class MultiResult {



    public List<SingleResult> results;

    public MultiResult(List<Record>[] results) {
        this.results = new ArrayList<>();
        for(List<Record> r: results) {
            this.results.add(new SingleResult(r));
        }
    }
}
