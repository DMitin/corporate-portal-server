package portal.pojo.neo4j;

import org.neo4j.driver.v1.Record;

import java.util.List;

/**
 * Created by Denis Mitin on 31.05.2016.
 */
public class Records {
    List<Record> records;

    public Records(List<Record> records) {
        this.records = records;
    }
}
