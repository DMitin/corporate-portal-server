package portal.pojo.neo4j;

import java.util.List;

/**
 * Created by Denis Mitin on 03.06.2016.
 */
public class SearchResult {
    public Integer totalRecords;
    public List<Neo4jEmployeeInterface> employeeList;

    public SearchResult(Integer totalRecords, List<Neo4jEmployeeInterface> employeeList) {
        this.totalRecords = totalRecords;
        this.employeeList = employeeList;
    }
}
