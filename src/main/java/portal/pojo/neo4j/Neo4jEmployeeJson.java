package portal.pojo.neo4j;

import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.NotImplementedException;
import org.neo4j.driver.v1.types.Node;
import portal.Utils;
import portal.enums.Department;
import portal.pojo.Entity;
import portal.pojo.employee.Employee;
import portal.pojo.employee.EmployeeSearch;
import portal.pojo.employee.Social;

import java.text.ParseException;
import java.util.*;

/**
 * Created by Denis Mitin on 19.07.2016.
 */
public class Neo4jEmployeeJson implements Neo4jEmployeeInterface {


    public Long id = null;
    public String firstName = "";
    public String lastName = "";
    public String middleName = "";
    public String email = "";
    public String skype = "";
    public String department = "";
    public String image = "";
    public String birthday = "";

    public String status = "new";
    public String ldapName;

    public String twitter = "";
    public String facebook = "";
    public String googlePlus = "";
    public String linkedin = "";
    public String instagram = "";
    public String vk = "";

    private JsonObject birthdayObject;

    private List<Entity> departments = new ArrayList<>();


    public Neo4jEmployeeJson() { }

    public Neo4jEmployeeJson(String ldapName) {
        this.ldapName = ldapName;
    }


    public Neo4jEmployeeInterface initValue(Node employeeNode, Node dayNode, Node monthNode, List<Object> departments)  {

        id = employeeNode.id();

        Map<String, Object> params = employeeNode.asMap();
        firstName = (String)params.get("firstName");
        lastName = (String)params.get("lastName");
        middleName = (String)params.get("middleName");
        email = (String)params.get("email");
        skype = (String)params.get("skype");
        //public String department = "";
        image = (String)params.get("image");
        birthday = (String)params.get("birthday");

        status = (String)params.get("status");
        ldapName = (String)params.get("ldapName");

        twitter = (String)params.get("twitter");
        facebook = (String)params.get("facebook");
        googlePlus = (String)params.get("googlePlus");
        linkedin = (String)params.get("linkedin");
        instagram = (String)params.get("instagram");
        vk = (String)params.get("vk");

        if (dayNode != null && monthNode != null) {
            Integer day = dayNode.get("number").asInt();
            Integer month = monthNode.get("number").asInt();
            birthdayObject = new JsonObject()
                    .put("day", day)
                    .put("month", month);
        }

        if (departments != null) {
            for (Object o: departments) {
                Node depNode = (Node)o;
                Entity department = Entity.initValue(depNode);
                this.departments.add(department);
            }
        }


        return this;
    }

    public Neo4jEmployeeInterface initValue(Employee employee, String ldapName) {
        String birthdayStr = employee.birthday;
        if (birthdayStr != null && !birthdayStr.trim().equals("")) {
            birthdayObject = new JsonObject();
            try {
                Date date = Utils.DATE_FORMAT.parse(birthdayStr);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int month = cal.get(Calendar.MONTH) + 1;
                int day = cal.get(Calendar.DAY_OF_MONTH);
                birthdayObject
                        .put("month", month)
                        .put("day", day);
            } catch (ParseException e) {
                birthdayObject.put("error", true);
            }
        }

        id = employee.id;
        firstName = employee.firstName;
        lastName = employee.lastName;
        middleName = employee.middleName;
        email = employee.email;
        skype = employee.skype;
        //public String department = "";
        image = employee.image;
        birthday = employee.birthday;

        status = employee.status;
        this.ldapName = ldapName;

        twitter = employee.social.twitter;
        facebook = employee.social.facebook;
        googlePlus = employee.social.googlePlus;
        linkedin = employee.social.linkedin;
        instagram = employee.social.instagram;
        vk = employee.social.vk;

        return this;
    }







    public String getBirthdayString() {
        if (birthday != null) {
            Integer day = birthdayObject.getInteger("number");
            Integer month = birthdayObject.getInteger("number");

            String birthdayStr = null;
            if (day != null && month != null) {
                String dayStr = day < 10 ? "0" + day : day.toString();
                String monthStr = month < 10 ? "0" + month : month.toString();
                birthdayStr = dayStr + "." + monthStr;
            }
            return  birthdayStr;
        } else {
            return "";
        }
    }

    public Employee toUiEmployee() {
        Employee e = new Employee();
        e.id = id;
        e.firstName = firstName;
        e.lastName = lastName;
        e.middleName = middleName;
        e.email = email;
        e.skype = skype;
        e.image = image;
        e.birthday = this.getBirthdayString();
        e.status = status;

        e.social = new Social();
        e.social.twitter = twitter;
        e.social.facebook = facebook;
        e.social.googlePlus = googlePlus;
        e.social.linkedin = linkedin;
        e.social.instagram = instagram;
        e.social.vk = vk;

        e.departments = departments;
        return e;
    }

    public EmployeeSearch toUiEmployeeSearch() {

        EmployeeSearch e = new EmployeeSearch();
        e.id = id;
        e.status = status;

        e.firstName = firstName;
        e.lastName = lastName;
        e.middleName = middleName;
        e.email = email;
        e.skype = skype;
        //e.department = department;
        e.image = image;

        return e;
    }

    public Long getId() {
        return id;
    }

    public JsonObject getFlatData() {
        JsonObject retVal = new JsonObject();

        retVal.put("firstName", firstName);
        retVal.put("lastName", lastName);
        retVal.put("middleName", middleName);

        retVal.put("email", email);
        retVal.put("skype", skype);
        //retVal.put("department", data.getString("department"));

        //retVal.put("image", image); не возвращается image!!!
        retVal.put("status", status);
        retVal.put("ldapName", ldapName);

        retVal.put("twitter", twitter);
        retVal.put("facebook", facebook);
        retVal.put("googlePlus", googlePlus);
        retVal.put("linkedin", linkedin);
        retVal.put("instagram", instagram);
        retVal.put("vk", vk);

        retVal.put("searchString", firstName.toLowerCase() + "#" + lastName.toLowerCase());
        return retVal;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public boolean isHR() {
        boolean hr = false;
        for(Entity d : departments) {
            if (d.name.equals(Department.HR)) {
                return true;
            }
        }
        return false;
    }


    public JsonObject getBirthday() {
        return birthdayObject;
    }
}
