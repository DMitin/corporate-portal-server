package portal.pojo.neo4j;

import org.neo4j.driver.v1.Value;

/**
 * Created by Denis Mitin on 02.06.2016.
 */
public class Request implements Neo4jRequest {

    public String query;
    public Value params;

    public Request() { }

    public Request(String query, Value params) {
        this.query = query;
        this.params = params;
    }
}
