package portal.pojo.neo4j;

/**
 * Created by Denis Mitin on 03.06.2016.
 */
public class SearchResultRecords {
    public Integer totalRecords;
    public Records records;
}
