package portal.pojo;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.neo4j.driver.v1.types.Node;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Сущность которая содержит только одно поле name
 * Под это условие попадают: Тэги, Проекты, Локации и др....
 */
public class Entity extends Common {

    public Long id;

    @NotNull
    public String name;

    @NotNull
    public String label;

    static Logger logger = LoggerFactory.getLogger(Entity.class.getName());

    public static Entity initValue(Node entityNode) {

        Map<String, Object> params = entityNode.asMap();
        Iterable<String> labels = entityNode.labels();

        Entity entity = new Entity();
        entity.id = entityNode.id();
        entity.name = (String)params.get("name");
        entity.label = getOneLabel(labels);

        return  entity;
    }

    public Entity() { }
    public Entity(String label, String  name) {
        this.label = label;
        this.name = name;
    }

    public boolean equalsIgnoreId(Entity other) {
        return (this.name.equals(other.name)) && (this.label.equals(other.label));
    }

    public boolean equals(Object obj) {return EqualsBuilder.reflectionEquals(this, obj); }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
