package portal.pojo;

import portal.pojo.employee.EmployeeSmall;

import java.util.ArrayList;

/**
 * Created by Denis Mitin on 24.05.2016.
 */
public class DayBirthdays {
    public int day;
    public ArrayList<EmployeeSmall> employees;
}
