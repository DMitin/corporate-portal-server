package portal.pojo;

import portal.pojo.employee.EmployeeSmall;

import java.util.ArrayList;

/**
 * Created by Denis Mitin on 24.05.2016.
 */
public class MonthBirthDays {
    public int day;
    public ArrayList<EmployeeSmall> employees;
}
