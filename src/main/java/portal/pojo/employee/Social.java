package portal.pojo.employee;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by Denis Mitin on 17.05.2016.
 */
public class Social implements Serializable {
    public String twitter = "";
    public String facebook = "";
    public String googlePlus = "";
    public String linkedin = "";
    public String instagram = "";
    public String vk = "";

    public boolean equals(Object obj) {
        boolean eq = EqualsBuilder.reflectionEquals(this, obj);
        return eq;
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
