package portal.pojo.employee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.driver.v1.types.Node;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by Denis Mitin on 25.05.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeSmall implements Serializable {

    public Long id ;
    public String status = "new";

    @NotNull
    @NotEmpty
    public String firstName = "";

    @NotNull
    @NotEmpty
    public String lastName = "";
    public String middleName = "";

    public String image = "";


    public static EmployeeSmall initValue(Node e) throws InvocationTargetException, IllegalAccessException {
        EmployeeSmall es = new EmployeeSmall();
        Map<String, Object> params = e.asMap();
        BeanUtils.populate(es, params);
        es.setId(e.id());
        return es;
    }

    public boolean equals(Object obj) { return EqualsBuilder.reflectionEquals(this, obj); }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
