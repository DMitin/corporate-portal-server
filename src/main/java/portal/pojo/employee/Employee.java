package portal.pojo.employee;

import portal.pojo.Entity;
import portal.validation.DateFormat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Mitin on 25.05.2016.
 */
public class Employee extends EmployeeSearch {

    @DateFormat
    public String birthday = "";

    public Social social = new Social();

    public List<Entity> departments = new ArrayList<>();
}
