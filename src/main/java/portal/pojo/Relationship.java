package portal.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Created by Denis Mitin on 09.08.2016.
 */
public class Relationship extends Common {

    public Long id;

    @NotNull
    public Long from;

    @NotNull
    public Long to;

    @NotNull
    @NotEmpty
    public String name;

    public Relationship() {}

    public Relationship(Long from, Long to, String name) {
        this.from = from;
        this.to = to;
        this.name = name;
    }

    public static Relationship initValue(org.neo4j.driver.v1.types.Relationship entityRel) {

        Map<String, Object> params = entityRel.asMap();
        String type = entityRel.type();

        Relationship relationship = new Relationship();
        relationship.id = entityRel.id();
        relationship.name = type;
        relationship.from = entityRel.startNodeId();
        relationship.to = entityRel.endNodeId();

        return  relationship;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
