package portal.sharedData;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import org.neo4j.driver.v1.types.Node;


import java.util.ArrayList;

/**
 * Created by denis on 10.09.16.
 */
public class NodeLabels {

    static Logger LOGGER = LoggerFactory.getLogger(NodeLabels.class.getName());


    public static void addEntityToCash(Node node, Vertx vertx) {

        long id = node.id();
        Iterable<String> labelsIterate = node.labels();
        ArrayList<String> labels = new ArrayList<String>();
        labelsIterate.forEach(l-> {
            labels.add(l);
        });
        if (labels.size() > 1) {
            LOGGER.error("Больше чем один label у Node");
        } else {
            SharedData sd = vertx.sharedData();
            LocalMap<Long, String> ids = sd.getLocalMap("Labels");
            ids.put(id, labels.get(0));
        }
    }

    public static String getLabelOfNode(Long nodeId, Vertx vertx) {
        SharedData sd = vertx.sharedData();
        LocalMap<Long, String> labels = sd.getLocalMap("Labels");
        return labels.get(nodeId);
    }
}


