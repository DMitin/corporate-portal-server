package portal.sharedData;

import io.vertx.core.Vertx;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import portal.pojo.UserData;

/**
 * Created by demi0416 on 08.09.2016.
 */
public class AuthUserData {


    public static UserData getUserData(String ldapName, Vertx vertx) {
            SharedData sd = vertx.sharedData();
            LocalMap<String, UserData> map = sd.getLocalMap("authUserData");
            UserData id = map.get(ldapName);
            return id;
    }

    public static void saveUserData(String ldapName, Vertx vertx, UserData userData) {
        SharedData sd = vertx.sharedData();
        LocalMap<String, UserData> map = sd.getLocalMap("authUserData");
        map.put(ldapName, userData);
    }

    public static void removeUserData(String ldapName, Vertx vertx) {
        SharedData sd = vertx.sharedData();
        LocalMap<String, UserData> map = sd.getLocalMap("authUserData");
        map.remove(ldapName);
    }
}
