package portal;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import portal.pojo.UserData;

import javax.validation.ConstraintViolation;
import java.text.SimpleDateFormat;
import java.util.Set;

/**
 * Created by DMitin on 25.03.2016.
 */
public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM");

    /**
     * Установка типовых заголовков в ответе
     * @param response
     * @return
     */
    public static HttpServerResponse setCommonHeaders(HttpServerResponse response) {
        response.putHeader("Content-type", "application/json; charset=utf-8");
        response.putHeader("Access-Control-Allow-Origin", "*");
        response.putHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        return response;
    }

    public static String logRequest(RoutingContext rc) {
/*
        MultiMap mm = rc.request().params();
        Set<String> names = mm.names();
         List<String> strs = names.stream().map(n -> {
             String val = mm.get(n);
             return n + ": " + val;
         }).collect(Collectors.toList());

        System.out.println(strs.toString());
*/
        StringBuilder sb = new StringBuilder();
        sb
                .append("url: ")
                .append(rc.request().uri())
                .append(System.getProperty("line.separator"))
                .append("body: ")
                .append(rc.getBodyAsString())
                .append(System.getProperty("line.separator"))
                .append("params: ")
                .append(rc.request().params())
                .append("headers: ")
                .append(rc.request().headers().names());

        return sb.toString();
    }

    public static String logResponse(RoutingContext rc, String response) {
        StringBuilder sb = new StringBuilder();
        sb
                .append("status: ")
                .append(rc.response().getStatusCode())
                .append(System.getProperty("line.separator"))
                .append("body: ")
                .append(response);

        return sb.toString();
    }






    /**
     * Отправка пользоватедю типового сообщения об ошибке
     * @param rc
     */
    public static void unknownError(RoutingContext rc) {
        String unknownServerError = "Неизвестная с ошибка";
        unknownError(rc, unknownServerError);

    }


    public static void unknownError(RoutingContext rc, String message) {
        LOGGER.fatal(message);
        rc.response()
                .setStatusCode(500)
                .end(message);
    }

    public static void sendValidationErrors(Set<ConstraintViolation<Object>> violations,
                                            RoutingContext rc) {
        JsonArray result = new JsonArray();

        for (ConstraintViolation violation : violations) {
            result.add(new JsonObject()
                    .put("path", violation.getPropertyPath().toString())
                    .put("message", violation.getMessage()));
        }
        rc.response().setStatusCode(405).end(result.toString());

    }
}
