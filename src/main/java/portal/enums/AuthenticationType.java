package portal.enums;

/**
 * Created by Denis Mitin on 29.07.2016.
 */
public class AuthenticationType {
    public static final String FAKE_TRUE = "fake-true";
    public static final String FAKE_FALSE = "fake-false";
    public static final String LDAP = "ldap";

    private AuthenticationType() { }
}
