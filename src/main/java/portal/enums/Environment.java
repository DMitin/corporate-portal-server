package portal.enums;

/**
 * Created by Denis Mitin on 29.07.2016.
 */
public class Environment {
    public static final String ENV_DEVELOPMENT = "development";
    public static final String ENV_PROD = "production";

    private Environment() { }
}
