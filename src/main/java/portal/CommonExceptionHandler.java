package portal;


import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;



public class CommonExceptionHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonExceptionHandler.class);

    @Override
    public void handle(RoutingContext rc) {

        Throwable throwable = rc.failure();
        if (!rc.response().ended()) {
            if (throwable != null) {
                rc.response()
                        .setStatusCode(500)
                        .end(throwable.getMessage());
            } else {
                rc.response()
                        .setStatusCode(500)
                        .end("ERROR ERROR!!!");
            }
        }
    }


}
