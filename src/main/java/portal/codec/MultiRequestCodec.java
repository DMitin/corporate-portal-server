package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.MultiRequest;

/**
 * Created by Denis Mitin on 02.06.2016.
 */
public class MultiRequestCodec implements MessageCodec<MultiRequest, MultiRequest> {
    @Override
    public void encodeToWire(Buffer buffer, MultiRequest multiRequest) {

    }

    @Override
    public MultiRequest decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public MultiRequest transform(MultiRequest multiRequest) {
        return multiRequest;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
