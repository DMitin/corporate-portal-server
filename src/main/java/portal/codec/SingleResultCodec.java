package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.SingleResult;

/**
 * Created by Denis Mitin on 01.06.2016.
 */
public class SingleResultCodec  implements MessageCodec<SingleResult, SingleResult> {
    @Override
    public void encodeToWire(Buffer buffer, SingleResult singleResult) {

    }

    @Override
    public SingleResult decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public SingleResult transform(SingleResult singleResult) {
        return singleResult;
    }

    @Override
    public String name() {
            return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
