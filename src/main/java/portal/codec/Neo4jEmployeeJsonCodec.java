package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.Neo4jEmployeeJson;

/**
 * Created by Denis Mitin on 01.06.2016.
 */
public class Neo4jEmployeeJsonCodec implements MessageCodec<Neo4jEmployeeJson, Neo4jEmployeeJson> {

    @Override
    public void encodeToWire(Buffer buffer, Neo4jEmployeeJson neo4jEmployee) {

    }

    @Override
    public Neo4jEmployeeJson decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public Neo4jEmployeeJson transform(Neo4jEmployeeJson neo4jEmployee) {
        return neo4jEmployee;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
