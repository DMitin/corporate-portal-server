package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.Entities;

/**
 * Created by Denis Mitin on 27.05.2016.
 */
public class EntitiesCodec implements MessageCodec<Entities, Entities> {
    @Override
    public void encodeToWire(Buffer buffer, Entities entities) {

    }

    @Override
    public Entities decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public Entities transform(Entities entities) {
        return entities;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
