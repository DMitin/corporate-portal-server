package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.SearchResultRecords;

/**
 * Created by Denis Mitin on 03.06.2016.
 */
public class SearchResponseRecordCodec implements MessageCodec<SearchResultRecords, SearchResultRecords> {
    @Override
    public void encodeToWire(Buffer buffer, SearchResultRecords searchResult) {

    }

    @Override
    public SearchResultRecords decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public SearchResultRecords transform(SearchResultRecords searchResult) {
        return searchResult;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
