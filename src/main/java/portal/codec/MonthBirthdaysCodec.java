package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.MonthBirthdays;

/**
 * Created by Denis Mitin on 25.05.2016.
 */
public class MonthBirthdaysCodec implements MessageCodec<MonthBirthdays, MonthBirthdays> {
    @Override
    public void encodeToWire(Buffer buffer, MonthBirthdays monthBirthdays) {

    }

    @Override
    public MonthBirthdays decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public MonthBirthdays transform(MonthBirthdays monthBirthdays) {
        return monthBirthdays;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
