package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

import java.util.ArrayList;

/**
 * Created by demi0416 on 20.09.2016.
 */
public class ArrayListCodec implements MessageCodec<ArrayList, ArrayList> {
    @Override
    public void encodeToWire(Buffer buffer, ArrayList entities) {

    }

    @Override
    public ArrayList decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public ArrayList transform(ArrayList entities) {
        return entities;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
