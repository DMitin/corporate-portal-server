package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.MultiResult;

/**
 * Created by Denis Mitin on 02.06.2016.
 */
public class MultiResultCodec implements MessageCodec<MultiResult, MultiResult> {
    @Override
    public void encodeToWire(Buffer buffer, MultiResult multiResult) {

    }

    @Override
    public MultiResult decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public MultiResult transform(MultiResult multiResult) {
        return multiResult;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
