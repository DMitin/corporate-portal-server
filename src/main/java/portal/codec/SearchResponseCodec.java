package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.SearchResult;

/**
 * Created by Denis Mitin on 03.06.2016.
 */
public class SearchResponseCodec implements MessageCodec<SearchResult, SearchResult> {
    @Override
    public void encodeToWire(Buffer buffer, SearchResult searchResult) {

    }

    @Override
    public SearchResult decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public SearchResult transform(SearchResult searchResult) {
        return searchResult;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
