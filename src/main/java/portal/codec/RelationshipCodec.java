package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.Relationship;

public class RelationshipCodec implements MessageCodec<Relationship, Relationship> {
    @Override
    public void encodeToWire(Buffer buffer, Relationship entities) {

    }

    @Override
    public Relationship decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public Relationship transform(Relationship entities) {
        return entities;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
