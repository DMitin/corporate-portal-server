package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.neo4j.Request;

/**
 * Created by Denis Mitin on 02.06.2016.
 */
public class RequestCodec implements MessageCodec<Request, Request> {
    @Override
    public void encodeToWire(Buffer buffer, Request request) {

    }

    @Override
    public Request decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public Request transform(Request request) {
        return request;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
