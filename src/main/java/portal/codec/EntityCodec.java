package portal.codec;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import portal.pojo.Entity;

/**
 * Created by Denis Mitin on 08.08.2016.
 */
public class EntityCodec implements MessageCodec<Entity, Entity> {
    @Override
    public void encodeToWire(Buffer buffer, Entity entities) {

    }

    @Override
    public Entity decodeFromWire(int pos, Buffer buffer) {
        return null;
    }

    @Override
    public Entity transform(Entity entities) {
        return entities;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
