package portal.func;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * Created by user on 17.04.2016.
 */
public class JsonArrayCollector implements Collector<JsonObject, JsonArray, JsonArray> {
    @Override
    public Supplier<JsonArray> supplier() {
        return ()-> new JsonArray();
    }

    @Override
    public BiConsumer<JsonArray, JsonObject> accumulator() {
        return (builder, t) -> builder.add(t);
    }

    @Override
    public BinaryOperator<JsonArray> combiner() {
        return (left, right) -> {
            left.addAll(right);
            return left;
        };
    }

    @Override
    public Function<JsonArray, JsonArray> finisher() {
        return (arr)->arr;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.UNORDERED);
    }

}
